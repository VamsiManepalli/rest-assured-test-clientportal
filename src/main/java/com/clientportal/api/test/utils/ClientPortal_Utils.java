package com.clientportal.api.test.utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

public class ClientPortal_Utils {

	private static String filename = "properties.properties";

	public static String getProperty(String key) {
		String value = null;
		Properties prop = new Properties();
		InputStream input = null;
		try {
			input = ClientPortal_Utils.class.getClassLoader().getResourceAsStream(filename);
			if (input == null) {
				throw new FileNotFoundException("Properties file not found.");
			}
			// load a properties file from class path, inside static method
			prop.load(input);
			value = prop.getProperty(key);
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return value;
	}
	
	public static Map<String, Object> getPropertyHashMap(String key) {
		Map<String, Object> map = new HashMap<>();
		//String value = null;
		Properties prop = new Properties();
		InputStream input = null;
		try {
			input = ClientPortal_Utils.class.getClassLoader().getResourceAsStream(filename);
			if (input == null) {
				throw new FileNotFoundException("Properties file not found.");
			}
			// load a properties file from class path, inside static method
			prop.load(input);
			Set<String> keys = new HashSet<String>(Arrays.asList(getProperty(key).split(",")));
			for(String value : keys) {
				int i = 1;
				String[] arrOfStr = new String[2];
				arrOfStr = value.split("=", 2);
				String mapKey = null;
	        	Object mapValue = null;
		        for (String a : arrOfStr){
		        	if(i%2 != 0) {
		        		mapKey = a;
		        	} else {
		        		mapValue = (Object) a;
		        	}
		        	i++;
		        }
		        map.put(mapKey, mapValue);
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return map;
	}
}
