package com.clientportal.api.test.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Color;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.clientportal.api.test.beans.ClientPortal_XLReportBean;

public class ClientPortal_XLRepostGenerator {

	XSSFWorkbook workbook = null;
	XSSFSheet sheet = null;
	Row row = null;

	
	public void createWorkBook() throws IOException {
		String xlfile=ClientPortal_Utils.getProperty("file.xlfile.path");
		FileInputStream fi=new FileInputStream(xlfile);
		workbook=new XSSFWorkbook(fi);
		sheet = workbook.getSheet("TestSteps");
		
//		sheet = workbook.createSheet("TestSteps");
//		if (workbook == null || sheet == null) {
//			workbook = new XSSFWorkbook();
//			
//			sheet = workbook.createSheet("TestSteps");
//		}
	}
	
	public void createXLFile(String fileName, List<ClientPortal_XLReportBean> testResults) {
		int rowNum = 0;
		generateSheetHeaders(sheet, rowNum++);
		
		for (ClientPortal_XLReportBean result : testResults) {
			row = sheet.createRow(rowNum++);
			int colNum = 0;
			Cell cell0 = row.createCell(colNum++);
			cell0.setCellValue(result.getTestName());
			Cell cell1 = row.createCell(colNum++);
			cell1.setCellValue(result.getStatus());
			Cell cell3 = row.createCell(colNum++);
			cell3.setCellValue(result.getErrorMessage());
			Cell cell4 = row.createCell(colNum++);
			cell4.setCellValue(result.getStartDate());
			Cell cell5 = row.createCell(colNum++);
			cell5.setCellValue(result.getEndDate());
			/*for (Object field : datatype) {
				Cell cell = row.createCell(colNum++);
				if (field instanceof String) {
					cell.setCellValue((String) field);
				} else if (field instanceof Integer) {
					cell.setCellValue((Integer) field);
				}
			}*/
		}
		
		try {
			FileOutputStream outputStream = new FileOutputStream(fileName);
			workbook.write(outputStream);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void closeWorkBook() {
		try {
			if(workbook != null) {
				workbook.close();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void generateSheetHeaders(XSSFSheet sheet, int rowNum) {
		row = sheet.createRow(rowNum);
		XSSFCellStyle style = workbook.createCellStyle();
		//style.setFillForegroundColor(IndexedColors.GREY_40_PERCENT.getIndex());
		XSSFFont font = workbook.createFont();
		font.setFontHeightInPoints((short) 15);
		font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
		style.setFont(font);                 
		int colNum = 0;
		Cell cell0 = row.createCell(colNum++);
		cell0.setCellValue("Test Name");
		cell0.setCellStyle(style);
		Cell cell1 = row.createCell(colNum++);
		cell1.setCellValue("Results");
		cell1.setCellStyle(style);
		Cell cell3 = row.createCell(colNum++);
		cell3.setCellValue("Error Message");
		cell3.setCellStyle(style);
		Cell cell4 = row.createCell(colNum++);
		cell4.setCellValue("Start Time");
		cell4.setCellStyle(style);
		Cell cell5 = row.createCell(colNum++);
		cell5.setCellValue("End Time");
		cell5.setCellStyle(style);
	}
}
