package com.clientportal.api.test.utils;

public enum ClientPortal_TestStatus {
	
	SUCCESS(1),
	FAILURE(2),
	SKIP(3),
	SUCCESS_PERCENTAGE_FAILURE(4), 
	STARTED(16);
	
	private int levelCode = -1;
	
	ClientPortal_TestStatus(int value) {
		this.levelCode = value;
	}
	
	public static String getTestStatusName(int levelCode) {
		for(ClientPortal_TestStatus e : ClientPortal_TestStatus.values()){
            if(levelCode == e.levelCode) return e.name();
        }
        return null;
    }
}
