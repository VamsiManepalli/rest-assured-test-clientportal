package com.clientportal.api.test.utils;

import java.nio.charset.Charset;
import java.util.Base64;
import java.util.Map;

import io.restassured.RestAssured;
import io.restassured.http.Header;
import io.restassured.http.Headers;
import io.restassured.specification.RequestSpecification;

public class ClientPortal_RestAssuredRequest {
	
	private static RequestSpecification httpRequest= null;
	public static RequestSpecification getInstance() {
		if(httpRequest == null){
			RestAssured.baseURI = "https://api.qa.newdirectionira.com";
			// Get HTTP request from rest assured
			httpRequest = RestAssured.given();
			//Set Basic authentication headers
			httpRequest.headers(new Headers(new Header("Authorization", "Basic "+getAuthBase64String())));
			// Specify content type
			httpRequest.contentType("application/json;charset=UTF-8");
		}	
		return httpRequest;
	}
	
	private static String getAuthBase64String() {
		Map<String, Object> params = ClientPortal_Utils.getPropertyHashMap("authenticationTest.request.params");
		String username = (String) params.get("username");
		String password = (String) params.get("password");
		String encoding = Base64.getEncoder().encodeToString((username+":"+password).getBytes(Charset.forName("UTF-8")));
		return encoding;		
	}
}
