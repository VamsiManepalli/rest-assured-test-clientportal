package com.clientportal.api.test;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.runners.Suite.SuiteClasses;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.clientportal.api.listeners.ClientPortal_TestsListener;
import com.clientportal.api.test.beans.ClientPortal_UserProfileBean;
import com.clientportal.api.test.utils.ClientPortal_RestAssuredRequest;
import com.clientportal.api.test.utils.ClientPortal_RestAssuredRequest_Sponsor;
import com.clientportal.api.test.utils.ClientPortal_Utils;
import com.clientportal.api.test.utils.ClientPortal_XLUtils;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

@SuiteClasses(value = { Frontend.class })
@Listeners(value = ClientPortal_TestsListener.class)
public class V2_Sponsor_BalanceInquiry 
{
	//Xlutils
	ClientPortal_XLUtils xl=new ClientPortal_XLUtils();	
	String xlfile=ClientPortal_Utils.getProperty("file.xlfile.path");
	String tcsheet = "TestData";			
			
	ObjectMapper mapper = null;
	Map<String, String> cookies = null;
	ClientPortal_UserProfileBean results = null;
	
	@BeforeClass
	public void loadResource() 
	{
		mapper = new ObjectMapper();
	}
			
				
			
	@Test(testName = "GET_Cash_available", priority = 0)
	public void GET_Cash_available() throws IOException
	{

		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest_Sponsor.getInstance();		
				
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
		int rc=ws.getLastRowNum();
		        
		        
		 for (int i = 0; i <=rc; i++) 
		 {
		      String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
		            
		      if (keyword.equalsIgnoreCase("Get_/api/v2/sponsors/clients/{accountNumber}/cash-available"))
		      {	
		    	  
		    	  String putpara1=xl.getCellData(xlfile, tcsheet, i, 4);
		    	  
		    	  httpRequest.basePath("/api/v2/sponsors/clients/"+putpara1+"/cash-available");
		    	  
		          	// Create map of input parameters and set to request body if any!
		           Map<String, Object> params = new HashMap<String, Object>();
		           params.put("accountNumber", putpara1);
						
					// Convert Java Object to JSON string
					httpRequest.body(mapper.writeValueAsString(params));
					
					// Send get request
					Response response = httpRequest.get();
					ResponseBody body = response.getBody();
					System.out.println(response.getBody().asString());
						
					// Validate status code
					response.then().assertThat().statusCode(200);
				}
		     }
		}
}
