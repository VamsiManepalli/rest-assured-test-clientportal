package com.clientportal.api.test;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.runners.Suite.SuiteClasses;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import com.clientportal.api.listeners.ClientPortal_TestsListener;
import com.clientportal.api.test.beans.ClientPortal_UserProfileBean;
import com.clientportal.api.test.utils.ClientPortal_RestAssuredRequest;
import com.clientportal.api.test.utils.ClientPortal_Utils;
import com.clientportal.api.test.utils.ClientPortal_XLUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

@SuiteClasses(value = { Frontend.class })
@Listeners(value = ClientPortal_TestsListener.class)
public class Frontend 
{

	//Xlutils
	ClientPortal_XLUtils xl=new ClientPortal_XLUtils();	
	String xlfile=ClientPortal_Utils.getProperty("file.xlfile.path");
	String tcsheet = "TestData";
	String oracleId="133693";
	String creditcardid="3424";
	
	ObjectMapper mapper = null;
	Map<String, String> cookies = null;
	ClientPortal_UserProfileBean results = null;
		
	@BeforeClass
	public void loadResource() {
		mapper = new ObjectMapper();
	}
	
	@Test(testName = "documents", priority = 1,enabled=false)
	public void documents() throws IOException {
		
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();
		httpRequest.basePath("/api/frontend/documents");
		
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();
        
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
            
            if (keyword.equalsIgnoreCase("/api/frontend/documents"))
            {	
            	String putpara1=xl.getCellData(xlfile, tcsheet, i, 4);
            	String putpara2=xl.getCellData(xlfile, tcsheet, i, 5);
            	String putpara3=xl.getCellData(xlfile, tcsheet, i, 6);
            	String putpara4=xl.getCellData(xlfile, tcsheet, i, 7);
            	String putpara5=xl.getCellData(xlfile, tcsheet, i, 8);
            	String putpara6=xl.getCellData(xlfile, tcsheet, i, 9);
            	String putpara7=xl.getCellData(xlfile, tcsheet, i, 10);
            	String putpara8=xl.getCellData(xlfile, tcsheet, i, 11);
            	String putpara9=xl.getCellData(xlfile, tcsheet, i, 12);
            	String putpara10=xl.getCellData(xlfile, tcsheet, i, 13);
            	String putpara11=xl.getCellData(xlfile, tcsheet, i, 14);
            	String putpara12=xl.getCellData(xlfile, tcsheet, i, 15);
            	String putpara13=xl.getCellData(xlfile, tcsheet, i, 16);
            	String putpara14=xl.getCellData(xlfile, tcsheet, i, 17);
            	String putpara15=xl.getCellData(xlfile, tcsheet, i, 18);
            	String putpara16=xl.getCellData(xlfile, tcsheet, i, 19);

				// Create map of input parameters and set to request body if any!
            	Map<String, Object> params = new HashMap<String, Object>();
            	Map<String, Object> params1 = new HashMap<String, Object>();
            	Map<String, Object> params2 = new HashMap<String, Object>();
				params.put("oracleId", putpara1);
				params1.put("userName", putpara2);
				params1.put("documentType",putpara3);
				params1.put("documentName", putpara4);
				params2.put("accountHolderName", putpara5);
				params2.put("accountSelect", putpara6);
				params2.put("phoneNumber", putpara7);
				params2.put("emailAddress", putpara8);
				params2.put("investmentDetailInvestmentIdentified", putpara9);
				params2.put("processTransaction", putpara10);
				params2.put("paymentMethod", putpara11);
				params2.put("paymentMethodFundingInformationAvailable", putpara12);
				params2.put("checkPayeeMailType", putpara13);
				params2.put("transactionFeeMethod", putpara14);
				params2.put("authenticateBy", putpara15);
				params2.put("phoneVerificationPhoneNumber", putpara16);
				params1.put("bd1", mapper.writeValueAsString(params2));
				params.put("data", mapper.writeValueAsString(params1));
				//System.out.println(mapper.writeValueAsString(params));
				
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));

				// Send post request
				Response response = httpRequest.post();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}

	@Test(testName = "couponsvalidate", priority = 2,enabled=false)
	public void couponsvalidate() throws IOException {
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();
		httpRequest.basePath("/api/frontend/coupons/validate");
		
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
	    int rc=ws.getLastRowNum();
	    	    
	    for (int i = 0; i <=rc; i++) 
	    {
	    	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
	        
	        if (keyword.equalsIgnoreCase("/api/frontend/coupons/validate"))
	        {	
	        	String putpara1=xl.getCellData(xlfile, tcsheet, i, 4);
	
				// Create map of input parameters and set to request body if any!
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("couponCode", putpara1);
							
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));
				// Send get request
				Response response = httpRequest.get();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
	    }
	}
	
	@Test(testName = "get_creditcards", priority = 3,enabled=false)
	public void get_creditcards() throws IOException {
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();
			
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
	    int rc=ws.getLastRowNum();	    
	    
	    for (int i = 0; i <=rc; i++) 
	    {
	    	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
	        
	        if (keyword.equalsIgnoreCase("get_creditcards"))
	        {	
	        	String putpara1=xl.getCellData(xlfile, tcsheet, i, 4);
	        	httpRequest.basePath("/api/frontend/creditcards/"+putpara1+"");
	        	
				// Create map of input parameters and set to request body if any!
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("oracleId", putpara1);
							
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));

				// Send get request
				Response response = httpRequest.get();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
	    }
	}
	
	@Test(testName = "Post_creditcards", priority = 4,enabled=false)
	public void Post_creditcards() throws IOException {
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();
		
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
	    int rc=ws.getLastRowNum();	    
	    
	    for (int i = 0; i <=rc; i++) 
	    {
	    	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
	        
	        if (keyword.equalsIgnoreCase("post_creditcards"))
	        {	
	        			
	        	String putpara1=xl.getCellData(xlfile, tcsheet, i, 4);
	        	String putpara2=xl.getCellData(xlfile, tcsheet, i, 5);
	        	String putpara3=xl.getCellData(xlfile, tcsheet, i, 6);
	        	String putpara4=xl.getCellData(xlfile, tcsheet, i, 7);	
	        	String putpara5=xl.getCellData(xlfile, tcsheet, i, 8);
	        	
	        	httpRequest.basePath("/api/frontend/creditcards/"+putpara1+"");	  
	        	
				// Create map of input parameters and set to request body if any!
				Map<String, Object> params = new HashMap<String, Object>();
				Map<String, Object> params1 = new HashMap<String, Object>();
				
				params.put("oracleId", putpara1);
				params1.put("cardHolderName", putpara2);
				params1.put("cardNumber", putpara3);
				params1.put("expiryDate", putpara4);
				params1.put("cvv", putpara5);
				params.put("CreditCard", mapper.writeValueAsString(params1));
								
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));

				// Send post request
				Response response = httpRequest.post();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
	    }
	}
	
	@Test(testName = "creditcards_payment", priority = 5,enabled=false)
	public void creditcards_payment() throws IOException {
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();
		httpRequest.basePath("/api/frontend/creditcards/payment/"+oracleId+"/"+creditcardid+"");
		
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
	    int rc=ws.getLastRowNum();	    
	    
	    for (int i = 0; i <=rc; i++) 
	    {
	    	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
	        
	        if (keyword.equalsIgnoreCase("post_creditcards"))
	        {	
	        	String putpara1=xl.getCellData(xlfile, tcsheet, i, 4);
	        	String putpara2=xl.getCellData(xlfile, tcsheet, i, 5);
	        	String putpara3=xl.getCellData(xlfile, tcsheet, i, 6);
	        		        	
	
				// Create map of input parameters and set to request body if any!
				Map<String, Object> params = new HashMap<String, Object>();
				Map<String, Object> params1 = new HashMap<String, Object>();
				
				params.put("oracleId", oracleId);
				params.put("creditCardId", creditcardid);
				params1.put("merchantRef", putpara1);
				params1.put("centsAmount", putpara2);
				params1.put("primaryAccountNumber", putpara3);
				params.put("CreditCard", mapper.writeValueAsString(params1));
				System.out.println(mapper.writeValueAsString(params));
				
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));

				// Send post request
				Response response = httpRequest.post();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
	    }
	}
	
	@Test(testName = "DELETE_creditcards", priority = 6,enabled=false)
	public void DELETE_creditcards() throws IOException {
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();
		httpRequest.basePath("/api/frontend/creditcards/payment/"+oracleId+"/"+creditcardid+"");
		
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
	    int rc=ws.getLastRowNum();	    
	    
	    for (int i = 0; i <=rc; i++) 
	    {
	    	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
	        
	        if (keyword.equalsIgnoreCase("/api/frontend/creditcards/{oracleId}/{creditCardId}"))
	        {	
	        	String putpara1=xl.getCellData(xlfile, tcsheet, i, 4);
	        	
				// Create map of input parameters and set to request body if any!
				Map<String, Object> params = new HashMap<String, Object>();
				
				
				params.put("oracleId", oracleId);
				params.put("creditCardId", creditcardid);
				
				System.out.println(mapper.writeValueAsString(params));
				
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));

				// Send post request
				Response response = httpRequest.post();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
	    }
	}
	
	@Test(testName = "Get_terms-and-conditions", priority = 7,enabled=false)
	public void Get_terms_and_conditions() throws IOException {
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();
		httpRequest.basePath("/api/frontend/terms-and-conditions");
		
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
	    int rc=ws.getLastRowNum();    
	    
	    for (int i = 0; i <=rc; i++) 
	    {
	    	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
	        
	        if (keyword.equalsIgnoreCase("/api/frontend/terms-and-conditions"))
	        {	
	        	String putpara1=xl.getCellData(xlfile, tcsheet, i, 4);
	        	String putpara2=xl.getCellData(xlfile, tcsheet, i, 5);
	
				// Create map of input parameters and set to request body if any!
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("termsAndConditionsName", putpara1);
				params.put("sponsorCode", putpara2);
							
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));

				// Send get request
				Response response = httpRequest.get();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
	    }
	}
	
	@Test(testName = "GET_credentials", priority = 8,enabled=false)
	public void GET_credentials() throws IOException {
		
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();
				
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
	    int rc=ws.getLastRowNum();    
	    
	    for (int i = 0; i <=rc; i++) 
	    {
	    	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
	        
	        if (keyword.equalsIgnoreCase("/api/frontend/credentials/{serviceType}"))
	        {	
	        	String servicetype=xl.getCellData(xlfile, tcsheet, i, 4);
	        	
	        	httpRequest.basePath("/api/frontend/credentials/"+servicetype+"");
	        	
				// Create map of input parameters and set to request body if any!
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("serviceType", servicetype);
				
							
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));

				// Send get request
				Response response = httpRequest.get();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
	    }
	}
	
	@Test(testName = "GET_sponsors_fees", priority = 9,enabled=false)
	public void GET_sponsors_fees() throws IOException {
		
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();		
		
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
	    int rc=ws.getLastRowNum();    
	    
	    for (int i = 0; i <=rc; i++) 
	    {
	    	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
	        
	        if (keyword.equalsIgnoreCase("/api/frontend/sponsors/{sponsorCode}/fees"))
	        {	
	        	String sponsorCode=xl.getCellData(xlfile, tcsheet, i, 4);
	        	
	    		httpRequest.basePath("/api/frontend/sponsors/"+sponsorCode+"/fees");
	    		
				// Create map of input parameters and set to request body if any!
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("sponsorCode", sponsorCode);
				
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));

				// Send get request
				Response response = httpRequest.get();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
	    }
	}
	
	@Test(testName = "POST_emails", priority = 10,enabled=false)
	public void POST_emails() throws IOException {
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();
		httpRequest.basePath("/api/frontend/emails");
		
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
	    int rc=ws.getLastRowNum();    
	    
	    for (int i = 0; i <=rc; i++) 
	    {
	    	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
	        
	        if (keyword.equalsIgnoreCase("/api/frontend/emails"))
	        {	
	        	String putparam1=xl.getCellData(xlfile, tcsheet, i, 4);
	        	String putparam2=xl.getCellData(xlfile, tcsheet, i, 5);
	        	String putparam3=xl.getCellData(xlfile, tcsheet, i, 6);
	        	String putparam4=xl.getCellData(xlfile, tcsheet, i, 7);
	        	String putparam5=xl.getCellData(xlfile, tcsheet, i, 8);
	        	String putparam6=xl.getCellData(xlfile, tcsheet, i, 9);
	        	String putparam7=xl.getCellData(xlfile, tcsheet, i, 10);
	        	
				// Create map of input parameters and set to request body if any!
				Map<String, Object> params = new HashMap<String, Object>();
				Map<String, Object> params1 = new HashMap<String, Object>();
				params.put("toAddresses", putparam1);
				params.put("fromAddress", putparam2);
				params.put("subject", putparam3);
				params.put("template", putparam4);
				params1.put("name", putparam5);
				params1.put("title", putparam6);
				params1.put("message", putparam7);
				params.put("templateData", mapper.writeValueAsString(params1));
				System.out.println(mapper.writeValueAsString(params));
				
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));

				// Send post request
				Response response = httpRequest.post();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
	    }
	}	
	
	@Test(testName = "GET_notification_logs", priority = 11)
	public void GET_notification_logs() throws IOException {
		
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();
				
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
	    int rc=ws.getLastRowNum();    
	    
	    for (int i = 0; i <=rc; i++) 
	    {
	    	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
	        
	        if (keyword.equalsIgnoreCase("get_/api/frontend/notification-logs"))
	        {	
	        	String putparam1=xl.getCellData(xlfile, tcsheet, i, 4); 
	        	String putparam2=xl.getCellData(xlfile, tcsheet, i, 5); 
	        	
	        	httpRequest.basePath("/api/frontend/notification-logs");
	        	httpRequest.queryParam("oracleId", putparam1);
	        	httpRequest.queryParam("notificationId", putparam2);
				
				// Send get request
				Response response = httpRequest.get();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
	    }
	}
	
	@Test(testName = "POST_notification_logs", priority = 13)
	public void POST_notification_logs() throws IOException {
		
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();
				
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
	    int rc=ws.getLastRowNum();    
	    
	    for (int i = 0; i <=rc; i++) 
	    {
	    	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
	        
	        if (keyword.equalsIgnoreCase("post_/api/frontend/notification-logs"))
	        {	
	        	String putparam1=xl.getCellData(xlfile, tcsheet, i, 4);
	        	String putparam2=xl.getCellData(xlfile, tcsheet, i, 5);
	        	int intputparam1=Integer.parseInt(putparam1);
				int intputparam2=Integer.parseInt(putparam2);
				
	        	httpRequest.basePath("/api/frontend/notification-logs");
	        	
				// Create map of input parameters and set to request body if any!
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("oracleId", intputparam1);
				params.put("notificationId", intputparam2);				
														
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));
				
				
				// Send post request
				Response response = httpRequest.post();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
	    }
	}
	
}

