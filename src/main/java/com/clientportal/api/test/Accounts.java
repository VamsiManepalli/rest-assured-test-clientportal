package com.clientportal.api.test;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.runners.Suite.SuiteClasses;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.clientportal.api.listeners.ClientPortal_TestsListener;
import com.clientportal.api.test.beans.ClientPortal_UserProfileBean;
import com.clientportal.api.test.utils.ClientPortal_RestAssuredRequest;
import com.clientportal.api.test.utils.ClientPortal_Utils;
import com.clientportal.api.test.utils.ClientPortal_XLUtils;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

@SuiteClasses(value = { Accounts.class })
@Listeners(value = ClientPortal_TestsListener.class)
public class Accounts {
	//Xlutils
	ClientPortal_XLUtils xl=new ClientPortal_XLUtils();	
	String xlfile=ClientPortal_Utils.getProperty("file.xlfile.path");
	String tcsheet = "TestData";
	public static String accountnumber="226	8311";
	public static String oracleId="133693";
	
	ObjectMapper mapper = null;
	Map<String, String> cookies = null;
	ClientPortal_UserProfileBean results = null;
		

	@BeforeClass
	public void loadResource() {
		mapper = new ObjectMapper();
	}
	@Test(testName = "GET_verbal_passwords", priority = 0,enabled=false)
	public void GET_verbal_passwords() throws IOException {
			
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();
			
			
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
	    int rc=ws.getLastRowNum();	        
	        
	    for (int i = 0; i <=rc; i++)
	     {
	    	 String keyword = xl.getCellData(xlfile,tcsheet , i, 2);	            
	    	 if (keyword.equalsIgnoreCase("/api/v1/accounts/{accountNumber}/verbal-passwords/templates/{oracleId}"))
	    	 {	
	            httpRequest.basePath("/api/v1/accounts/"+accountnumber+"/verbal-passwords/templates/"+oracleId+"");
	            	            	
				// Create map of input parameters and set to request body if any!
	            Map<String, Object> params = new HashMap<String, Object>();
				params.put("accountNumber", accountnumber);
				params.put("oracleId", oracleId);
									
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));
				System.out.println(mapper.writeValueAsString(params));
				
				// Send get request
				Response response = httpRequest.get();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
					
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
	     }
	}
	
	@Test(testName = "GET_accounts_addresses", priority = 1)
	public void GET_accounts_addresses() throws IOException {
			
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();			
			
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
	    int rc=ws.getLastRowNum();
	        
	        
	     for (int i = 0; i <=rc; i++)
	     {
	    	 String keyword = xl.getCellData(xlfile,tcsheet , i, 2);	            
	    	 if (keyword.equalsIgnoreCase("get/api/accounts/{accountNumber}/addresses"))
	    	 {	
	            httpRequest.basePath("/api/accounts/"+accountnumber+"/addresses");
	            	            	
				// Create map of input parameters and set to request body if any!
	            Map<String, Object> params = new HashMap<String, Object>();
				params.put("accountNumber", accountnumber);
													
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));
				
				// Send get request
				Response response = httpRequest.get();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
					
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
	     }
	   }
	
	@Test(testName = "POST_accounts_addresses", priority = 2)
	public void POST_accounts_addresses() throws IOException {
			
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();			
			
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
	    int rc=ws.getLastRowNum();
	        
	        
	     for (int i = 0; i <=rc; i++)
	     {
	    	 String keyword = xl.getCellData(xlfile,tcsheet , i, 2);	            
	    	 if (keyword.equalsIgnoreCase("post/api/accounts/{accountNumber}/addresses"))
	    	 {	
	            httpRequest.basePath("/api/accounts/"+accountnumber+"/addresses");
	            String putparam3,putparam4,putparam5,putparam6,putparam7,putparam8,putparam9,putparam10,putparam11;
	            
	            putparam3=xl.getCellData(xlfile, tcsheet, i, 4);
	            System.out.println(putparam3);
	            putparam4=xl.getCellData(xlfile, tcsheet, i, 5);
	            putparam5=xl.getCellData(xlfile, tcsheet, i, 6);
	            putparam6=xl.getCellData(xlfile, tcsheet, i, 7);
	            putparam7=xl.getCellData(xlfile, tcsheet, i, 8);
	            putparam8=xl.getCellData(xlfile, tcsheet, i, 9);
	            putparam9=xl.getCellData(xlfile, tcsheet, i, 10);
	            putparam10=xl.getCellData(xlfile, tcsheet, i, 11);
	            putparam11=xl.getCellData(xlfile, tcsheet, i, 12);
	            
				// Create map of input parameters and set to request body if any!
	            Map<String, Object> params = new HashMap<String, Object>();
	            Map<String, Object> params1 = new HashMap<String, Object>();
	            Map<String, Object> params2 = new HashMap<String, Object>();
	            Map<String, Object> params3 = new HashMap<String, Object>();
	            
				params.put("accountNumber", accountnumber);
				params1.put("oracleAccountId", oracleId);
				params1.put("accountNumber", accountnumber);
				params2.put("street", putparam3);
				params2.put("city", putparam4);
				params2.put("state", putparam5);
				params2.put("addressType", putparam6);
				params3.put("street", putparam7);
				params3.put("city", putparam8);
				params3.put("state", putparam9);
				params3.put("zip", putparam10);
				params3.put("addressType", putparam11);
				params2.put("addresses", mapper.writeValueAsString(params3));
				params1.put("Address", mapper.writeValueAsString(params2));
				params.put("accountNumber", mapper.writeValueAsString(params1));
				
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));
				System.out.println(mapper.writeValueAsString(params));
				
				// Send post request
				Response response = httpRequest.post();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
					
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
	     }
	}
	
	@Test(testName = "GET_api_accounts", priority = 3)
	public void GET_api_accounts() throws IOException {
			
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();			
		httpRequest.basePath("/api/accounts");
		// Send get request
		Response response = httpRequest.get();
		ResponseBody body = response.getBody();
		System.out.println(response.getBody().asString());
					
		// Validate status code
		response.then().assertThat().statusCode(200);
		
	     
	}
	
	@Test(testName = "POST_api_accounts", priority = 4)
	public void POST_api_accounts() throws IOException {
			
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();			
		httpRequest.basePath("/api/accounts");
	            	            	
		// Create map of input parameters and set to request body if any!
	        Map<String, Object> params = new HashMap<String, Object>();
			params.put("accountNumber", accountnumber);
													
		// Convert Java Object to JSON string
			httpRequest.body(mapper.writeValueAsString(params));
			System.out.println(mapper.writeValueAsString(params));
				
		// Send post request
			Response response = httpRequest.post();
			ResponseBody body = response.getBody();
			System.out.println(response.getBody().asString());
					
		// Validate status code
			response.then().assertThat().statusCode(200);
	}
	
	@Test(testName = "GET_api_accounts_reports", priority = 5)
	public void GET_api_accounts_reports() throws IOException {
			
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();			
		httpRequest.basePath("/api/accounts/reports");
		
		// Send get request
			Response response = httpRequest.get();
			ResponseBody body = response.getBody();
			System.out.println(response.getBody().asString());
					
		// Validate status code
			response.then().assertThat().statusCode(200);
	}
	
	@Test(testName = "GET_api_accounts_accountNumber", priority = 6)
	public void GET_api_accounts_accountNumber() throws IOException {
			
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();			
		httpRequest.basePath("/api/accounts/"+accountnumber+"");
	            	            	
		// Create map of input parameters and set to request body if any!
	        Map<String, Object> params = new HashMap<String, Object>();
	        params.put("accountNumber", accountnumber);
													
		// Convert Java Object to JSON string
	       httpRequest.body(mapper.writeValueAsString(params));
				
		// Send get request
			Response response = httpRequest.get();
			ResponseBody body = response.getBody();
			System.out.println(response.getBody().asString());
					
			// Validate status code
			response.then().assertThat().statusCode(200);
		}	
	
	@Test(testName = "GET_notification_settings", priority = 7)
	public void GET_notification_settings() throws IOException {
			
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();			
		httpRequest.basePath("/api/accounts/"+accountnumber+"/notification_settings");
	            	            	
		// Create map of input parameters and set to request body if any!
	        Map<String, Object> params = new HashMap<String, Object>();
	        params.put("accountNumber", accountnumber);
													
		// Convert Java Object to JSON string
	       httpRequest.body(mapper.writeValueAsString(params));
				
		// Send get request
			Response response = httpRequest.get();
			ResponseBody body = response.getBody();
			System.out.println(response.getBody().asString());
					
			// Validate status code
			response.then().assertThat().statusCode(200);
		}	
	
	@Test(testName = "POST_notification_settings", priority = 8)
	public void POST_notification_settings() throws IOException {
			
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();			
		
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
	    int rc=ws.getLastRowNum();
	        
	     for (int i = 0; i <=rc; i++)
	     {
	    	 String keyword = xl.getCellData(xlfile,tcsheet , i, 2);	            
	    	 if (keyword.equalsIgnoreCase("get/api/accounts/{accountNumber}/notification_settings"))
	    	 {	
				httpRequest.basePath("/api/accounts/"+accountnumber+"/notification_settings");
			        String putparam1=xl.getCellData(xlfile, tcsheet, i, 4);    	            	
				// Create map of input parameters and set to request body if any!
			        Map<String, Object> params = new HashMap<String, Object>();
			        params.put("accountNumber", accountnumber);
			        params.put("Account", putparam1);
															
				// Convert Java Object to JSON string
			       httpRequest.body(mapper.writeValueAsString(params));
						
				// Send post request
					Response response = httpRequest.post();
					ResponseBody body = response.getBody();
					System.out.println(response.getBody().asString());
							
					// Validate status code
					response.then().assertThat().statusCode(200);
				}	
	     }
	}
	
	@Test(testName = "GET_accounts_emails", priority = 9)
	public void GET_accounts_emails() throws IOException {
			
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();			
		httpRequest.basePath("/api/accounts/"+accountnumber+"/emails");
	            	            	
		// Create map of input parameters and set to request body if any!
	        Map<String, Object> params = new HashMap<String, Object>();
	        params.put("accountNumber", accountnumber);
													
		// Convert Java Object to JSON string
	       httpRequest.body(mapper.writeValueAsString(params));
				
		// Send get request
			Response response = httpRequest.get();
			ResponseBody body = response.getBody();
			System.out.println(response.getBody().asString());
					
		// Validate status code
			response.then().assertThat().statusCode(200);
		}	
	
	@Test(testName = "POST_accounts_emails", priority = 8)
	public void POST_accounts_emails() throws IOException {
			
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();			
		
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
	    int rc=ws.getLastRowNum();
	        
	        
	     for (int i = 0; i <=rc; i++)
	     {
	    	 String keyword = xl.getCellData(xlfile,tcsheet , i, 2);	            
	    	 if (keyword.equalsIgnoreCase("/api/accounts/{accountNumber}/emails"))
	    	 {	
				httpRequest.basePath("/api/accounts/"+accountnumber+"/emails");
			        String putparam1=xl.getCellData(xlfile, tcsheet, i, 4);    
			        String putparam2=xl.getCellData(xlfile, tcsheet, i, 5);    
			        
				// Create map of input parameters and set to request body if any!
			        Map<String, Object> params = new HashMap<String, Object>();
			        Map<String, Object> params1 = new HashMap<String, Object>();
			        params.put("accountNumber", accountnumber);
			        params1.put("email", putparam1);
			        params1.put("ip", putparam2);
			        params.put("homePhoneNumber", mapper.writeValueAsString(params1));
			        															
				// Convert Java Object to JSON string
			       httpRequest.body(mapper.writeValueAsString(params));
						
				// Send post request
					Response response = httpRequest.post();
					ResponseBody body = response.getBody();
					System.out.println(response.getBody().asString());
							
					// Validate status code
					response.then().assertThat().statusCode(200);
				}	
	     }
	}
	
	@Test(testName = "GET_accounts_phones", priority = 10)
	public void GET_accounts_phones() throws IOException {
			
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();			
		httpRequest.basePath("/api/accounts/"+accountnumber+"/phones");
	            	            	
		// Create map of input parameters and set to request body if any!
	        Map<String, Object> params = new HashMap<String, Object>();
	        params.put("accountNumber", accountnumber);
													
		// Convert Java Object to JSON string
	       httpRequest.body(mapper.writeValueAsString(params));
				
		// Send get request
			Response response = httpRequest.get();
			ResponseBody body = response.getBody();
			System.out.println(response.getBody().asString());
					
			// Validate status code
			response.then().assertThat().statusCode(200);
		}	
	
	@Test(testName = "POST_accounts_phones", priority = 11)
	public void POST_accounts_phones() throws IOException {
			
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();			
		
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
	    int rc=ws.getLastRowNum();
	        
	        
	     for (int i = 0; i <=rc; i++)
	     {
	    	 String keyword = xl.getCellData(xlfile,tcsheet , i, 2);	            
	    	 if (keyword.equalsIgnoreCase("/api/accounts/{accountNumber}/phones"))
	    	 {	
				httpRequest.basePath("/api/accounts/"+accountnumber+"/phones");
			        String putparam1=xl.getCellData(xlfile, tcsheet, i, 4);    
			        String putparam2=xl.getCellData(xlfile, tcsheet, i, 5);    
			        
				// Create map of input parameters and set to request body if any!
			        Map<String, Object> params = new HashMap<String, Object>();
			        Map<String, Object> params1 = new HashMap<String, Object>();
			        params.put("accountNumber", accountnumber);
			        params1.put("homePhoneNumber", putparam1);
			        params1.put("ip", putparam2);
			        params.put("homePhoneNumber", mapper.writeValueAsString(params1));
			        															
				// Convert Java Object to JSON string
			       httpRequest.body(mapper.writeValueAsString(params));
				System.out.println(mapper.writeValueAsString(params));	
				
				// Send post request
					Response response = httpRequest.post();
					ResponseBody body = response.getBody();
					System.out.println(response.getBody().asString());
							
					// Validate status code
					response.then().assertThat().statusCode(200);
				}	
	     }
	}
	
	@Test(testName = "POST_accounts_PayBills", priority = 12)
	public void POST_accounts_PayBills() throws IOException {
			
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();			
		
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
	    int rc=ws.getLastRowNum();
	        
	        
	     for (int i = 0; i <=rc; i++)
	     {
	    	 String keyword = xl.getCellData(xlfile,tcsheet , i, 2);	            
	    	 if (keyword.equalsIgnoreCase("/api/accounts/{accountNumber}/payBills"))
	    	 {	
				httpRequest.basePath("/api/accounts/"+accountnumber+"/payBills");
			        
				String putparam1=xl.getCellData(xlfile, tcsheet, i, 4); 			         
			        
				// Create map of input parameters and set to request body if any!
			        Map<String, Object> params = new HashMap<String, Object>();
			       
			        params.put("accountNumber", accountnumber);
			        params.put("BillPay", putparam1);
			       			        															
				// Convert Java Object to JSON string
			       httpRequest.body(mapper.writeValueAsString(params));
								
				// Send post request
					Response response = httpRequest.post();
					ResponseBody body = response.getBody();
					System.out.println(response.getBody().asString());
							
					// Validate status code
					response.then().assertThat().statusCode(200);
				}	
	     }
	}	
	
	@Test(testName = "GET_accounts_distributions", priority = 13)
	public void GET_accounts_distributions() throws IOException {
			
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();			
		httpRequest.basePath("/api/accounts/"+accountnumber+"/distributions");
	            	            	
		// Create map of input parameters and set to request body if any!
	        Map<String, Object> params = new HashMap<String, Object>();
	        params.put("accountNumber", accountnumber);
													
		// Convert Java Object to JSON string
	       httpRequest.body(mapper.writeValueAsString(params));
				
		// Send get request
			Response response = httpRequest.get();
			ResponseBody body = response.getBody();
			System.out.println(response.getBody().asString());
					
			// Validate status code
			response.then().assertThat().statusCode(200);
		}	
	
	@Test(testName = "POST_accounts_distributions", priority = 14)
	public void POST_accounts_distributions() throws IOException {
			
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();			
		httpRequest.basePath("/api/accounts/"+accountnumber+"/distributions");
	            	            	
		// Create map of input parameters and set to request body if any!
	        Map<String, Object> params = new HashMap<String, Object>();
	        params.put("accountNumber", accountnumber);
													
		// Convert Java Object to JSON string
	       httpRequest.body(mapper.writeValueAsString(params));
				
		// Send post request
			Response response = httpRequest.post();
			ResponseBody body = response.getBody();
			System.out.println(response.getBody().asString());
					
			// Validate status code
			response.then().assertThat().statusCode(200);
		}	
	
	@Test(testName = "GET_clientDocuments_incomplete", priority = 15)
	public void GET_clientDocuments_incomplete() throws IOException {
			
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();			
		
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
	    int rc=ws.getLastRowNum();
	        
	        
	     for (int i = 0; i <=rc; i++)
	     {
	    	 String keyword = xl.getCellData(xlfile,tcsheet , i, 2);	            
	    	 if (keyword.equalsIgnoreCase("/api/v1/accounts/{accountId}/clientDocuments/incomplete/{accountNumber}"))
	    	 {	
				
			        String accountId=xl.getCellData(xlfile, tcsheet, i, 4);    
			           
			        httpRequest.basePath("/api/v1/accounts/"+accountId+"/clientDocuments/incomplete/"+accountnumber+"");
			        
				// Create map of input parameters and set to request body if any!
			        Map<String, Object> params = new HashMap<String, Object>();
			        params.put("accountId", accountId);
			        params.put("accountNumber", accountnumber);
			       
			        															
				// Convert Java Object to JSON string
			       httpRequest.body(mapper.writeValueAsString(params));
				System.out.println(mapper.writeValueAsString(params));	
				
				// Send get request
					Response response = httpRequest.get();
					ResponseBody body = response.getBody();
					System.out.println(response.getBody().asString());
							
					// Validate status code
					response.then().assertThat().statusCode(200);
				}	
	     }
	}
	
	@Test(testName = "POST_clientDocuments_statuses", priority = 16)
	public void POST_clientDocuments_statuses() throws IOException {
			
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();			
		
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
	    int rc=ws.getLastRowNum();
	        
	        
	     for (int i = 0; i <=rc; i++)
	     {
	    	 String keyword = xl.getCellData(xlfile,tcsheet , i, 2);	            
	    	 if (keyword.equalsIgnoreCase("/api/accounts/{accountId}/clientDocuments/{documentId}/statuses"))
	    	 {	
				
			        String accountId=xl.getCellData(xlfile, tcsheet, i, 4);
			        String documentId=xl.getCellData(xlfile, tcsheet, i, 5);
			        String Status=xl.getCellData(xlfile, tcsheet, i, 6);
			           
			        httpRequest.basePath("/api/accounts/"+accountId+"/clientDocuments/"+documentId+"/statuses");
			        
				// Create map of input parameters and set to request body if any!
			        Map<String, Object> params = new HashMap<String, Object>();
			        Map<String, Object> params1 = new HashMap<String, Object>();
			        params.put("accountId", accountId);
			        params.put("documentId", documentId);
			        params1.put("status", Status);
			        params.put("Status",mapper.writeValueAsString(params1));
			       
			        															
				// Convert Java Object to JSON string
			       httpRequest.body(mapper.writeValueAsString(params));
					
				
				// Send post request
					Response response = httpRequest.post();
					ResponseBody body = response.getBody();
					System.out.println(response.getBody().asString());
							
					// Validate status code
					response.then().assertThat().statusCode(200);
				}	
	     }
	}
	
	@Test(testName = "POST_clientDocuments_test", priority = 17)
	public void POST_clientDocuments_test() throws IOException {
			
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();			
		
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
	    int rc=ws.getLastRowNum();
	        
	        
	     for (int i = 0; i <=rc; i++)
	     {
	    	 String keyword = xl.getCellData(xlfile,tcsheet , i, 2);	            
	    	 if (keyword.equalsIgnoreCase("/api/accounts/{accountId}/clientDocuments/{documentId}/test"))
	    	 {	
				
			        String accountId=xl.getCellData(xlfile, tcsheet, i, 4);
			        String documentId=xl.getCellData(xlfile, tcsheet, i, 5);
			        String Status=xl.getCellData(xlfile, tcsheet, i, 6);
			           
			        httpRequest.basePath("/api/accounts/"+accountId+"/clientDocuments/"+documentId+"/test");
			        
				// Create map of input parameters and set to request body if any!
			        Map<String, Object> params = new HashMap<String, Object>();
			        Map<String, Object> params1 = new HashMap<String, Object>();
			        params.put("accountId", accountId);
			        params.put("documentId", documentId);
			        params1.put("status", Status);
			        params.put("Status",mapper.writeValueAsString(params1));
			       
			        															
				// Convert Java Object to JSON string
			       httpRequest.body(mapper.writeValueAsString(params));
					
				
				// Send post request
					Response response = httpRequest.post();
					ResponseBody body = response.getBody();
					System.out.println(response.getBody().asString());
							
					// Validate status code
					response.then().assertThat().statusCode(200);
				}	
	     }
	}
	
	@Test(testName = "POST_login_credentials", priority = 18)
	public void POST_login_credentials() throws IOException {
			
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();			
		
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
	    int rc=ws.getLastRowNum();
	        
	        
	     for (int i = 0; i <=rc; i++)
	     {
	    	 String keyword = xl.getCellData(xlfile,tcsheet , i, 2);	            
	    	 if (keyword.equalsIgnoreCase("/api/v1/accounts/{accountNumber}/oracle-login-credentials/{oracleId}"))
	    	 {	
				
			        String putparam1=xl.getCellData(xlfile, tcsheet, i, 4);
			        String putparam2=xl.getCellData(xlfile, tcsheet, i, 5);
			        String putparam3=xl.getCellData(xlfile, tcsheet, i, 6);
			           
			        httpRequest.basePath("/api/v1/accounts/"+accountnumber+"/oracle-login-credentials/"+oracleId+"");
			        
				// Create map of input parameters and set to request body if any!
			        Map<String, Object> params = new HashMap<String, Object>();
			        Map<String, Object> params1 = new HashMap<String, Object>();
			        params.put("accountNumber", accountnumber);
			        params.put("oracleId", oracleId);
			        params1.put("login", putparam1);
			        params1.put("password", putparam2);
			        params1.put("ip", putparam3);
			        params.put("data",mapper.writeValueAsString(params1));
			       
			        															
				// Convert Java Object to JSON string
			       httpRequest.body(mapper.writeValueAsString(params));
					
				
				// Send post request
					Response response = httpRequest.post();
					ResponseBody body = response.getBody();
					System.out.println(response.getBody().asString());
							
					// Validate status code
					response.then().assertThat().statusCode(200);
				}	
	     }
	}
	
	@Test(testName = "GET_accounts_transactions", priority = 19)
	public void GET_accounts_transactions() throws IOException {
			
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();			
		
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
	    int rc=ws.getLastRowNum();
	        
	        
	     for (int i = 0; i <=rc; i++)
	     {
	    	 String keyword = xl.getCellData(xlfile,tcsheet , i, 2);	            
	    	 if (keyword.equalsIgnoreCase("/api/accounts/{accountNumber}/transactions"))
	    	 {	
				
			        String putparam1=xl.getCellData(xlfile, tcsheet, i, 4);
			        String putparam2=xl.getCellData(xlfile, tcsheet, i, 5);
			        String putparam3=xl.getCellData(xlfile, tcsheet, i, 6);
			           
			        httpRequest.basePath("/api/accounts/"+accountnumber+"/transactions");
			        
				// Create map of input parameters and set to request body if any!
			        Map<String, Object> params = new HashMap<String, Object>();
			       
			        params.put("accountNumber", accountnumber);
			        params.put("transactionType", putparam1);
			        params.put("limit", putparam2);
			        params.put("offset", putparam3);
			        															
				// Convert Java Object to JSON string
			       httpRequest.body(mapper.writeValueAsString(params));
					
				
				// Send get request
					Response response = httpRequest.get();
					ResponseBody body = response.getBody();
					System.out.println(response.getBody().asString());
							
					// Validate status code
					response.then().assertThat().statusCode(200);
				}	
	     }
	}
	
	@Test(testName = "GET_transactions_attachments", priority = 20)
	public void GET_transactions_attachments() throws IOException {
			
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();			
		
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
	    int rc=ws.getLastRowNum();
	        
	        
	     for (int i = 0; i <=rc; i++)
	     {
	    	 String keyword = xl.getCellData(xlfile,tcsheet , i, 2);	            
	    	 if (keyword.equalsIgnoreCase("/api/accounts/transactions/{transactionId}/attachments/{attachmentId}"))
	    	 {	
				
			        String transactionId=xl.getCellData(xlfile, tcsheet, i, 4);
			        String attachmentId=xl.getCellData(xlfile, tcsheet, i, 5);
			       
			        httpRequest.basePath("/api/accounts/transactions/"+transactionId+"/attachments/"+attachmentId+"");
			        
				// Create map of input parameters and set to request body if any!
			        Map<String, Object> params = new HashMap<String, Object>();
			       
			        params.put("transactionId", transactionId);
			        params.put("attachmentId", attachmentId);
			       														
				// Convert Java Object to JSON string
			       httpRequest.body(mapper.writeValueAsString(params));
					
				
				// Send get request
					Response response = httpRequest.get();
					ResponseBody body = response.getBody();
					System.out.println(response.getBody().asString());
							
					// Validate status code
					response.then().assertThat().statusCode(200);
				}	
	     }
	}
}
