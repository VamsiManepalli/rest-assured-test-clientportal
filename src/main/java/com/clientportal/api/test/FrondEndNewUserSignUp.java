package com.clientportal.api.test;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Array;
import java.util.HashMap;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import org.junit.runners.Suite.SuiteClasses;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.clientportal.api.listeners.ClientPortal_TestsListener;
import com.clientportal.api.test.beans.AuthResponseBody;
import com.clientportal.api.test.beans.ClientPortal_UserProfileBean;
import com.clientportal.api.test.utils.ClientPortal_RestAssuredRequest;
import com.clientportal.api.test.utils.ClientPortal_RestAssuredRequest_Sponsor;
import com.clientportal.api.test.utils.ClientPortal_Utils;
import com.clientportal.api.test.utils.ClientPortal_XLUtils;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;
@SuiteClasses(value = { authenticate.class })
@Listeners(value = ClientPortal_TestsListener.class)

public class FrondEndNewUserSignUp 
{

	//Xlutils
	ClientPortal_XLUtils xl=new ClientPortal_XLUtils();	
	String xlfile=ClientPortal_Utils.getProperty("file.xlfile.path");
	String tcsheet = "TestData";
	private static String usersignuptoken;
	
	
	ObjectMapper mapper = null;
	Map<String, String> cookies = null;
	ClientPortal_UserProfileBean results = null;
		
	@BeforeClass
	public void loadResource() {
		mapper = new ObjectMapper();
	}
	@Test(testName = "clientusersignupstep1", priority = 0)
	public void clientuserSignupstep1() throws IOException
	{
	RequestSpecification httpRequest=ClientPortal_RestAssuredRequest.getInstance();
	FileInputStream fi=new FileInputStream(xlfile);
	Workbook wb=new XSSFWorkbook(fi);
	org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
    int rc=ws.getLastRowNum();
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
            if (keyword.equalsIgnoreCase("post_/api/frontend/sponsors/{sponsorCode}/clients/sign-up"))
            {	
            	String putpara1=xl.getCellData(xlfile, tcsheet, i, 4);
            	String putpara2=xl.getCellData(xlfile, tcsheet, i, 5);
            	String putpara3=xl.getCellData(xlfile, tcsheet, i, 6);
            	String putpara4=xl.getCellData(xlfile, tcsheet, i, 7);
            	String putpara5=xl.getCellData(xlfile, tcsheet, i, 8);
            	String putpara6=xl.getCellData(xlfile, tcsheet, i, 9);
            	
            	
            	httpRequest.basePath("/api/frontend/sponsors/"+putpara1+"/clients/sign-up");

				// Create map of input parameters and set to request body if any!
            	//Map<String, Object> params1 = new HashMap<String, Object>();
            	Map<String, Object> params = new HashMap<String, Object>();
            	//params.put("sponsorCode", putpara1);
				params.put("emailAddress", putpara2);
				params.put("firstName", putpara3);
				params.put("middleName",putpara4);
				params.put("lastName", putpara5);
				params.put("password", putpara6);
				//params.put("SponsorClientBasic", params1);
				
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));

				// Send post request
				Response response = httpRequest.post();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				usersignuptoken = mapper.readValue(response.getBody().asString(), AuthResponseBody.class).getData().getuserSignupToken();
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}
	@Test(testName = "clientusersignupstep2", priority = 1)
	public void sponsorWhiteLabelNewUserSignUpStep2() throws IOException 
	{
		RequestSpecification httpRequest=ClientPortal_RestAssuredRequest.getInstance();
		
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
        	
            if (keyword.equalsIgnoreCase("patch_/api/frontend/sponsors/{sponsorCode}/clients/sign-up"))
            {	
            	String putpara1=xl.getCellData(xlfile, tcsheet, i, 4);
            	String putpara2=xl.getCellData(xlfile, tcsheet, i, 5);
            	String putpara3=xl.getCellData(xlfile, tcsheet, i, 6);
            	String putpara4=xl.getCellData(xlfile, tcsheet, i, 7);
            	String putpara5=xl.getCellData(xlfile, tcsheet, i, 8);
            	String putpara6=xl.getCellData(xlfile, tcsheet, i, 9);
            	String putpara7=xl.getCellData(xlfile, tcsheet, i, 10);
            	String putpara8=xl.getCellData(xlfile, tcsheet, i, 11);
            	String putpara9=xl.getCellData(xlfile, tcsheet, i, 12);
            	String putpara10=xl.getCellData(xlfile, tcsheet, i, 13);
            	String putpara11=xl.getCellData(xlfile, tcsheet, i, 14);
            	String putpara12=xl.getCellData(xlfile, tcsheet, i, 15);
            	String putpara13=xl.getCellData(xlfile, tcsheet, i, 16);
            	String putpara14=xl.getCellData(xlfile, tcsheet, i, 17);
            	String putpara15=xl.getCellData(xlfile, tcsheet, i, 18);
            	String putpara16=xl.getCellData(xlfile, tcsheet, i, 19);
            	String putpara17=xl.getCellData(xlfile, tcsheet, i, 20);
            	String putpara18=xl.getCellData(xlfile, tcsheet, i, 21);
            	String putpara19=xl.getCellData(xlfile, tcsheet, i, 22);
            	String putpara20=xl.getCellData(xlfile, tcsheet, i, 23);
            	String putpara21=xl.getCellData(xlfile, tcsheet, i, 24);
            	String putpara22=xl.getCellData(xlfile, tcsheet, i, 25);
            	String putpara23=xl.getCellData(xlfile, tcsheet, i, 26);
            	String putpara24=xl.getCellData(xlfile, tcsheet, i, 27);
            	
            	
            	httpRequest.basePath("/api/frontend/sponsors/"+putpara1+"/clients/sign-up");

				// Create map of input parameters and set to request body if any!
            	
            	Map<String, Object> params = new HashMap<String, Object>();
            	
            	//Map<String, Object> params2 = new HashMap<String, Object>();
            	
            	httpRequest.header("userSignupToken",usersignuptoken);
            	//params.put("userSignupToken", usersignuptoken);
                params.put("dateOfBirth", putpara2);
            	params.put("SSN",putpara3);
            	params.put("maritalStatusId", 198);
            	params.put("phone", putpara5);
            	params.put("streetAddress1", putpara6);
            	params.put("streetCity",putpara7);
            	params.put("streetState", putpara8);
            	params.put("streetZipCode", putpara9);
            	params.put("mailingAddress1", putpara10);
            	params.put("mailingCity", putpara11);
            	params.put("mailingState", putpara12);
            	params.put("mailingZipCode",putpara13);
            	params.put("billingAddress1", putpara14);
            	params.put("billingCity", putpara15);
            	params.put("billingState", putpara16);
            	params.put("billingZipCode", putpara17);
            	params.put("emailNotification", true);
            	params.put("annualFeeMethodId", 585);
            	params.put("feeScheduleId", 20);
            	params.put("termsAgreed", true);
            	params.put("feeScheduleAgreed", true);
            	params.put("accountTypeId", 1);
            	params.put("transactionFeeMethod", 604);
            	
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));
				System.out.println(params);
            	

				// Send post request
				Response response = httpRequest.patch();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				
				
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}
	@Test(testName = "FrontEndNewUserSignUpStorageOptions", priority = 2)
	public void sponsorWhiteLabelNewUserSignUpStorageOptions() throws IOException 
	{
		RequestSpecification httpRequest=ClientPortal_RestAssuredRequest.getInstance();
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
        	
            if (keyword.equalsIgnoreCase("/api/frontend/sponsors/{sponsorCode}/clients/sign-up/storage-options"))
            {	
            	String putpara1=xl.getCellData(xlfile, tcsheet, i, 4);
            	String putpara2=xl.getCellData(xlfile, tcsheet, i, 5);
            	httpRequest.basePath("/api/frontend/sponsors/"+putpara1+"/clients/sign-up/storage-options");

				// Create map of input parameters and set to request body if any!
            	
            	Map<String, Object> params = new HashMap<String, Object>();
            	Map<String, Object> params1 = new HashMap<String, Object>();
            	
            	//Map<String, Object> params2 = new HashMap<String, Object>();
            	
            	httpRequest.header("userSignupToken",usersignuptoken);
            	params.put("depository", Integer.parseInt(putpara2));
            	
				// Convert Java Object to JSON string
            	
				httpRequest.body(mapper.writeValueAsString(params));

				// Send post request
				Response response = httpRequest.post();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}
	@Test(testName = "FrontEndNewUserSignUpFinalize", priority = 3)
	public void sponsorWhiteLabelNewUserSignUpFinalize() throws IOException 
	{
		RequestSpecification httpRequest=ClientPortal_RestAssuredRequest.getInstance();
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
        	
            if (keyword.equalsIgnoreCase("/api/frontend/sponsors/{sponsorCode}/clients/sign-up/finalize"))
            {	
            	String putpara1=xl.getCellData(xlfile, tcsheet, i, 4);
            	String putpara2=xl.getCellData(xlfile, tcsheet, i, 5);
            	String putpara3=xl.getCellData(xlfile, tcsheet, i, 6);
            	String putpara4=xl.getCellData(xlfile, tcsheet, i, 7);
            	String putpara5=xl.getCellData(xlfile, tcsheet, i, 8);
            	String putpara6=xl.getCellData(xlfile, tcsheet, i, 9);
            	
            	httpRequest.basePath("/api/frontend/sponsors/"+putpara1+"/clients/sign-up/finalize");

				// Create map of input parameters and set to request body if any!
            	
            	Map<String, Object> params = new HashMap<String, Object>();
            	
            	httpRequest.header("userSignupToken",usersignuptoken);
            	httpRequest.header("couponCode",putpara2);
            	params.put("cardHolderName", putpara3);
            	params.put("cardNumber", putpara4);
            	params.put("expiryDate", putpara5);
            	params.put("cvv", putpara6);
            	
				// Convert Java Object to JSON string
            	
				httpRequest.body(mapper.writeValueAsString(params));
				System.out.println(params);

				// Send post request
				Response response = httpRequest.post();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}
}
