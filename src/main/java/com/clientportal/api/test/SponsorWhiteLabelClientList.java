package com.clientportal.api.test;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.runners.Suite.SuiteClasses;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.clientportal.api.listeners.ClientPortal_TestsListener;
import com.clientportal.api.test.beans.ClientPortal_UserProfileBean;
import com.clientportal.api.test.utils.ClientPortal_RestAssuredRequest;
import com.clientportal.api.test.utils.ClientPortal_RestAssuredRequest_Sponsor;
import com.clientportal.api.test.utils.ClientPortal_Utils;
import com.clientportal.api.test.utils.ClientPortal_XLUtils;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;
@SuiteClasses(value = { authenticate.class })
@Listeners(value = ClientPortal_TestsListener.class)
public class SponsorWhiteLabelClientList
{

	//Xlutils
	ClientPortal_XLUtils xl=new ClientPortal_XLUtils();	
	String xlfile=ClientPortal_Utils.getProperty("file.xlfile.path");
	String tcsheet = "TestData";
	
	
	ObjectMapper mapper = null;
	Map<String, String> cookies = null;
	ClientPortal_UserProfileBean results = null;
		
	@BeforeClass
	public void loadResource() {
		mapper = new ObjectMapper();
	}
	@Test(testName = "SponsorClientsTest", priority = 0)
	public void sponsorClients() throws IOException 
	{
		//RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();
		RequestSpecification httpRequest=ClientPortal_RestAssuredRequest_Sponsor.getInstance();
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
        	
            if (keyword.equalsIgnoreCase("/api/v2/sponsors/clients"))
            {	
            	String putpara1=xl.getCellData(xlfile, tcsheet, i, 4);
            	int intputpara1=Integer.parseInt(putpara1);
            	String putpara2=xl.getCellData(xlfile, tcsheet, i, 5);
            	int intputpara2=Integer.parseInt(putpara2);
            	httpRequest.basePath("/api/v2/sponsors/clients");

				// Create map of input parameters and set to request body if any!	
            	Map<String, Object> params = new HashMap<String, Object>();
            	params.put("offset", intputpara1);
            	params.put("limit", intputpara2);
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));

				// Send post request
				Response response = httpRequest.get();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}
	@Test(testName = "SponsorPendingTransactionsTest", priority = 1)
	public void sponsorPendingTransactions() throws IOException 
	{
		//RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();
		RequestSpecification httpRequest=ClientPortal_RestAssuredRequest_Sponsor.getInstance();
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
        	
            if (keyword.equalsIgnoreCase("/api/v2/sponsors/pending-transactions"))
            {	
            	String putpara1=xl.getCellData(xlfile, tcsheet, i, 4);
            	int intputpara1=Integer.parseInt(putpara1);
            	String putpara2=xl.getCellData(xlfile, tcsheet, i, 5);
            	int intputpara2=Integer.parseInt(putpara2);
            	httpRequest.basePath("/api/v2/sponsors/pending-transactions");

				// Create map of input parameters and set to request body if any!
            	Map<String, Object> params = new HashMap<String, Object>();
            	params.put("offset", intputpara1);
            	params.put("limit", intputpara2);
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));

				// Send post request
				Response response = httpRequest.get();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}
	@Test(testName = "SponsorTransactionStatusTest", priority = 2)
	public void sponsorTransactionStatus() throws IOException 
	{
		//RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();
		RequestSpecification httpRequest=ClientPortal_RestAssuredRequest_Sponsor.getInstance();
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
        	
            if (keyword.equalsIgnoreCase("/api/v2/sponsors/pending-transaction-status"))
            {	
            	String putpara1=xl.getCellData(xlfile, tcsheet, i, 4);
            	int intputpara1=Integer.parseInt(putpara1);
            	httpRequest.basePath("/api/v2/sponsors/pending-transaction-status");
            	
				// Create map of input parameters and set to request body if any!
            	Map<String, Object> params = new HashMap<String, Object>();
            	params.put("transactionId", intputpara1);
            	
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));

				// Send post request
				Response response = httpRequest.post();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}
	@Test(testName = "SponsorCompletedTransactionsTest", priority = 3)
	public void sponsorCompletedTransactions() throws IOException 
	{
		//RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();
		RequestSpecification httpRequest=ClientPortal_RestAssuredRequest_Sponsor.getInstance();
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
        	
            if (keyword.equalsIgnoreCase("/api/v2/sponsors/completed-transactions"))
            {	
            	String putpara1=xl.getCellData(xlfile, tcsheet, i, 4);
            	int intputpara1=Integer.parseInt(putpara1);
            	String putpara2=xl.getCellData(xlfile, tcsheet, i, 5);
            	int intputpara2=Integer.parseInt(putpara2);
            	httpRequest.basePath("/api/v2/sponsors/completed-transactions");

				// Create map of input parameters and set to request body if any!
            	Map<String, Object> params = new HashMap<String, Object>();
            	params.put("offset", intputpara1);
            	params.put("limit", intputpara2);
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));

				// Send post request
				Response response = httpRequest.get();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}
}
