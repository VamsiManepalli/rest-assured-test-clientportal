package com.clientportal.api.test;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.runners.Suite.SuiteClasses;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.clientportal.api.listeners.ClientPortal_TestsListener;
import com.clientportal.api.test.beans.AuthResponseBody;
import com.clientportal.api.test.beans.ClientPortal_UserProfileBean;
import com.clientportal.api.test.utils.ClientPortal_RestAssuredRequest;

import com.clientportal.api.test.utils.ClientPortal_Utils;
import com.clientportal.api.test.utils.ClientPortal_XLUtils;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

@SuiteClasses(value = { authenticate.class })
@Listeners(value = ClientPortal_TestsListener.class)
public class Depositories
{

	//Xlutils
	ClientPortal_XLUtils xl=new ClientPortal_XLUtils();	
	String xlfile=ClientPortal_Utils.getProperty("file.xlfile.path");
	String tcsheet = "TestData";
	
	
	ObjectMapper mapper = null;
	Map<String, String> cookies = null;
	ClientPortal_UserProfileBean results = null;
	private static String usersignuptoken;
		
	@BeforeClass
	public void loadResource() {
		mapper = new ObjectMapper();
	}
	@Test(testName = "GetDepositories", priority = 0)
	public void getDepositoriesTest() throws IOException 
	{
		RequestSpecification httpRequest=ClientPortal_RestAssuredRequest.getInstance();
		
            	httpRequest.basePath("/api/depositories");
				// Send get request
				Response response = httpRequest.get();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				// Validate status code
				response.then().assertThat().statusCode(200);
			
	}
	@Test(testName = "PostDepositories", priority = 1)
	public void PostDepositoriesTest() throws IOException 
	{
		RequestSpecification httpRequest=ClientPortal_RestAssuredRequest.getInstance();
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
        	
            if (keyword.equalsIgnoreCase("Post_/api/depositories"))
            {	
            	String putpara1=xl.getCellData(xlfile, tcsheet, i, 4);
            	
            	httpRequest.basePath("/api/depositories");

				// Create map of input parameters and set to request body if any!
            	Map<String, Object> params = new HashMap<String, Object>();
            	Map<String, Object> params1 = new HashMap<String, Object>();
            	params1.put("depositoryName", putpara1);
            	params.put("depository", params1);
            	
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));
				
				// Send post request
				Response response = httpRequest.post();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}
	@Test(testName = "DeleteDepositories", priority = 2,enabled=false)
	public void deleteDepositories() throws IOException 
	{
		RequestSpecification httpRequest=ClientPortal_RestAssuredRequest.getInstance();
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
        	
            if (keyword.equalsIgnoreCase("Delete_/api/depositories/{id}"))
            {	
            	String putpara1=xl.getCellData(xlfile, tcsheet, i, 4);
            	int id=Integer.parseInt(putpara1);
            	
            	httpRequest.basePath("/api/depositories/"+id+"");

				// Create map of input parameters and set to request body if any!
            	Map<String, Object> params = new HashMap<String, Object>();
            	Map<String, Object> params1 = new HashMap<String, Object>();
            	params1.put("id", id);
								
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));

				// Send post request
				Response response = httpRequest.delete();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}
	@Test(testName = "GetDepositoriesid", priority = 3,enabled=false)
	public void getDepositoriesid() throws IOException 
	{
		RequestSpecification httpRequest=ClientPortal_RestAssuredRequest.getInstance();
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
        	
            if (keyword.equalsIgnoreCase("Get_/api/depositories/{id}"))
            {	
            	String putpara1=xl.getCellData(xlfile, tcsheet, i, 4);
            	int id=Integer.parseInt(putpara1);
            	
            	httpRequest.basePath("/api/depositories/"+id+"");

				// Create map of input parameters and set to request body if any!
            	Map<String, Object> params = new HashMap<String, Object>();
            	
            	params.put("id", id);
								
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));

				// Send post request
				Response response = httpRequest.get();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}
	@Test(testName = "PatchDepositoresid", priority = 4,enabled=false)
	public void patchDepositoresid() throws IOException 
	{
		RequestSpecification httpRequest=ClientPortal_RestAssuredRequest.getInstance();
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
        	
            if (keyword.equalsIgnoreCase("Patch_/api/depositories/{id}"))
            {	
            	String putpara1=xl.getCellData(xlfile, tcsheet, i, 4);
            	String putpara2=xl.getCellData(xlfile, tcsheet, i, 5);
            	int id=Integer.parseInt(putpara1);
            	
            	httpRequest.basePath("/api/depositories/"+id+"");

				// Create map of input parameters and set to request body if any!
            	Map<String, Object> params = new HashMap<String, Object>();
            	Map<String, Object> params1 = new HashMap<String, Object>();
            	params.put("id", id);
            	params1.put("depositoryName", putpara2);
            	params.put("depository", params1);
            				
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));

				// Send post request
				Response response = httpRequest.patch();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}
	@Test(testName = "GetDepositoresidBranches", priority = 5,enabled=false)
	public void getdepositoresidBranchesTest() throws IOException 
	{
		RequestSpecification httpRequest=ClientPortal_RestAssuredRequest.getInstance();
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
        	
            if (keyword.equalsIgnoreCase("Get_/api/depositories/{depositoryId}/branches"))
            {	
            	String putpara1=xl.getCellData(xlfile, tcsheet, i, 4);
            	String putpara2=xl.getCellData(xlfile, tcsheet, i, 5);
            	int id=Integer.parseInt(putpara1);
            	
            	httpRequest.basePath("/api/depositories/"+id+"/branches");

				// Create map of input parameters and set to request body if any!
            	Map<String, Object> params = new HashMap<String, Object>();
            	params.put("depositoryId", id);
								
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));

				// Send post request
				Response response = httpRequest.get();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}
	@Test(testName = "PostDepositoresidBranches", priority = 6,enabled=false)
	public void postDepositoresidBranchesTest() throws IOException 
	{
		RequestSpecification httpRequest=ClientPortal_RestAssuredRequest.getInstance();
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
        	
            if (keyword.equalsIgnoreCase("Post_/api/depositories/{depositoryId}/branches"))
            {	
            	String putpara1=xl.getCellData(xlfile, tcsheet, i, 4);
            	int depositoryId=Integer.parseInt(putpara1);
            	
            	String putpara2=xl.getCellData(xlfile, tcsheet, i, 5);
            	String putpara3=xl.getCellData(xlfile, tcsheet, i, 6);
            	String putpara4=xl.getCellData(xlfile, tcsheet, i, 7);
            	
            	
            	httpRequest.basePath("/api/depositories/"+depositoryId+"/branches");

				// Create map of input parameters and set to request body if any!
            	Map<String, Object> params = new HashMap<String, Object>();
            	Map<String, Object> params1 = new HashMap<String, Object>();
            	params.put("depositoryId", depositoryId);
            	params1.put("branchName	", putpara2);
            	params1.put("address", putpara3);
            	params1.put("city", putpara4);
            	params1.put("stateOrProvince", null);
            	params1.put("zipCode", "");
            	params1.put("country", null);
            	params1.put("contactName", null);
            	params1.put("contactEmail", null);
            	params1.put("contactPhone", null);
            	params.put("depositorybranch", params1);
            	
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));

				// Send post request
				Response response = httpRequest.post();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}
	@Test(testName = "DeleteDepositoresidBranchesid", priority = 7,enabled=false)
	public void deletedepositoresidBranchesidTest() throws IOException 
	{
		RequestSpecification httpRequest=ClientPortal_RestAssuredRequest.getInstance();
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
        	
            if (keyword.equalsIgnoreCase("Delete_/api/depositories/{depositoryId}/branches/{id}"))
            {	
            	String putpara1=xl.getCellData(xlfile, tcsheet, i, 4);
            	String putpara2=xl.getCellData(xlfile, tcsheet, i, 5);
            	int depositoryId=Integer.parseInt(putpara1);
            	int id=Integer.parseInt(putpara2);
            	
            	httpRequest.basePath("/api/depositories/"+depositoryId+"/branches/"+id+"");

				// Create map of input parameters and set to request body if any!
            	Map<String, Object> params = new HashMap<String, Object>();
            	params.put("depositoryId", depositoryId);
            	params.put("id", id);
            	
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));

				// Send post request
				Response response = httpRequest.delete();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}
	@Test(testName = "GetDepositoresidBranchesid", priority = 8,enabled=false)
	public void getdepositoresidBranchesidTest() throws IOException 
	{
		RequestSpecification httpRequest=ClientPortal_RestAssuredRequest.getInstance();
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
        	
            if (keyword.equalsIgnoreCase("Get_/api/depositories/{depositoryId}/branches/{id}"))
            {	
            	String putpara1=xl.getCellData(xlfile, tcsheet, i, 4);
            	String putpara2=xl.getCellData(xlfile, tcsheet, i, 5);
            	int depositoryId=Integer.parseInt(putpara1);
            	int id=Integer.parseInt(putpara2);
            	
            	httpRequest.basePath("/api/depositories/"+depositoryId+"/branches/"+id+"");

				// Create map of input parameters and set to request body if any!
            	Map<String, Object> params = new HashMap<String, Object>();
            	params.put("depositoryId", depositoryId);
            	params.put("id", id);
            	
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));

				// Send post request
				Response response = httpRequest.get();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}
	@Test(testName = "PatchDepositoresidBranchesid", priority = 9,enabled=false)
	public void patchdepositoresidBranchesidTest() throws IOException 
	{
		RequestSpecification httpRequest=ClientPortal_RestAssuredRequest.getInstance();
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
        	
            if (keyword.equalsIgnoreCase("Patch_/api/depositories/{depositoryId}/branches/{id}"))
            {	
            	String putpara1=xl.getCellData(xlfile, tcsheet, i, 4);
            	String putpara2=xl.getCellData(xlfile, tcsheet, i, 5);
            	String putpara3=xl.getCellData(xlfile, tcsheet, i, 6);
            	String putpara4=xl.getCellData(xlfile, tcsheet, i, 7);
            	String putpara5=xl.getCellData(xlfile, tcsheet, i, 8);
            	int depositoryId=Integer.parseInt(putpara1);
            	int id=Integer.parseInt(putpara2);
            	
            	httpRequest.basePath("/api/depositories/"+depositoryId+"/branches/"+id+"");

				// Create map of input parameters and set to request body if any!
            	Map<String, Object> params = new HashMap<String, Object>();
            	Map<String, Object> params1 = new HashMap<String, Object>();
            	params.put("depositoryId", depositoryId);
            	params.put("id", id);
            	params1.put("branchName	", putpara3);
            	params1.put("address", putpara4);
            	params1.put("city", putpara5);
            	params1.put("stateOrProvince", null);
            	params1.put("zipCode", "");
            	params1.put("country", null);
            	params1.put("contactName", null);
            	params1.put("contactEmail", null);
            	params1.put("contactPhone", null);
            	params.put("depository branch", params1);
            	
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));

				// Send post request
				Response response = httpRequest.patch();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}
	@Test(testName = "GetDepositoresidBranchesidfeeSchedules", priority = 10,enabled=false)
	public void getdepositoresidBranchesidfeeSchedulesTest() throws IOException 
	{
		RequestSpecification httpRequest=ClientPortal_RestAssuredRequest.getInstance();
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
        	
            if (keyword.equalsIgnoreCase("Get_/api/depositories/{depositoryId}/branches/{depositoryBranchId}/fee-schedules"))
            {	
            	String putpara1=xl.getCellData(xlfile, tcsheet, i, 4);
            	String putpara2=xl.getCellData(xlfile, tcsheet, i, 5);
            	int depositoryId=Integer.parseInt(putpara1);
            	int depositoryBranchId=Integer.parseInt(putpara2);
            	
            	httpRequest.basePath("/api/depositories/"+depositoryId+"/branches/"+depositoryBranchId+"/fee-schedules");

				// Create map of input parameters and set to request body if any!
            	Map<String, Object> params = new HashMap<String, Object>();
            	params.put("depositoryId", depositoryId);
            	params.put("depositoryBranchId", depositoryBranchId);
            	
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));

				// Send post request
				Response response = httpRequest.get();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}
	@Test(testName = "PostDepositoresidBranchesidfeeSchedules", priority = 11,enabled=false)
	public void postdepositoresidBranchesidfeeSchedulesTest() throws IOException 
	{
		RequestSpecification httpRequest=ClientPortal_RestAssuredRequest.getInstance();
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
        	
            if (keyword.equalsIgnoreCase("Post_/api/depositories/{depositoryId}/branches/{depositoryBranchId}/fee-schedules"))
            {	
            	String putpara1=xl.getCellData(xlfile, tcsheet, i, 4);
            	String putpara2=xl.getCellData(xlfile, tcsheet, i, 5);
            	String putpara3=xl.getCellData(xlfile, tcsheet, i, 6);
            	int depositoryId=Integer.parseInt(putpara1);
            	int depositoryBranchId=Integer.parseInt(putpara2);
            	
            	httpRequest.basePath("/api/depositories/"+depositoryId+"/branches/"+depositoryBranchId+"/fee-schedules");

				// Create map of input parameters and set to request body if any!
            	Map<String, Object> params = new HashMap<String, Object>();
            	
            	params.put("depositoryId", depositoryId);
            	params.put("depositoryBranchId", depositoryBranchId);
            	params.put("depositoryFeeScheduleName", putpara3);
            	params.put("basisPoints", null);
            	params.put("rangeDescription", null);
            	params.put( "minimumFee", null);
            	params.put("frequency", null);
            	params.put("sortOrder", null);
            	params.put("handlingFee", null);
            	params.put("additionalChargesDescription", null);
            	params.put( "minimumNDIPortion", null);
            	params.put("ndiPortionCalculation", null);
            	params.put("depositoryTypeId", null);
            	
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));

				// Send post request
				Response response = httpRequest.post();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}
	@Test(testName = "DeleteDepositoresidBranchesidfeeSchedules", priority = 12,enabled=false)
	public void deletedepositoresidBranchesidfeeSchedulesTest() throws IOException 
	{
		RequestSpecification httpRequest=ClientPortal_RestAssuredRequest.getInstance();
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
        	
            if (keyword.equalsIgnoreCase("Delete_/api/depositories/{depositoryId}/branches/{depositoryBranchId}/fee-schedules"))
            {	
            	String putpara1=xl.getCellData(xlfile, tcsheet, i, 4);
            	String putpara2=xl.getCellData(xlfile, tcsheet, i, 5);
            	String putpara3=xl.getCellData(xlfile, tcsheet, i, 6);
            	int depositoryId=Integer.parseInt(putpara1);
            	int depositoryBranchId=Integer.parseInt(putpara2);
            	int id=Integer.parseInt(putpara3);
            	httpRequest.basePath("/api/depositories/"+depositoryId+"/branches/"+depositoryBranchId+"/fee-schedules/"+id+"");

				// Create map of input parameters and set to request body if any!
            	Map<String, Object> params = new HashMap<String, Object>();
            	
            	params.put("depositoryId", depositoryId);
            	params.put("depositoryBranchId", depositoryBranchId);
            	params.put("id", id);
            	
            	
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));

				// Send delete request
				Response response = httpRequest.delete();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}
	@Test(testName = "PatchDepositoresidBranchesidfeeSchedules", priority = 13,enabled=false)
	public void patchdepositoresidBranchesidfeeSchedulesTest() throws IOException 
	{
		RequestSpecification httpRequest=ClientPortal_RestAssuredRequest.getInstance();
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
        	
            if (keyword.equalsIgnoreCase("Patch_/api/depositories/{depositoryId}/branches/{depositoryBranchId}/fee-schedules"))
            {	
            	String putpara1=xl.getCellData(xlfile, tcsheet, i, 4);
            	String putpara2=xl.getCellData(xlfile, tcsheet, i, 5);
            	String putpara3=xl.getCellData(xlfile, tcsheet, i, 6);
            	String putpara4=xl.getCellData(xlfile, tcsheet, i, 7);
            	int depositoryId=Integer.parseInt(putpara1);
            	int depositoryBranchId=Integer.parseInt(putpara2);
            	int id=Integer.parseInt(putpara3);
            	httpRequest.basePath("/api/depositories/"+depositoryId+"/branches/"+depositoryBranchId+"/fee-schedules/"+id+"");

				// Create map of input parameters and set to request body if any!
            	Map<String, Object> params = new HashMap<String, Object>();
            	
            	params.put("depositoryId", depositoryId);
            	params.put("depositoryBranchId", depositoryBranchId);
            	params.put("id", id);
            	params.put("depositoryFeeScheduleName", putpara4);
            	params.put("basisPoints", null);
            	params.put("rangeDescription", null);
            	params.put( "minimumFee", null);
            	params.put("frequency", null);
            	params.put("sortOrder", null);
            	params.put("handlingFee", null);
            	params.put("additionalChargesDescription", null);
            	params.put( "minimumNDIPortion", null);
            	params.put("ndiPortionCalculation", null);
            	params.put("depositoryTypeId", null);
            	
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));

				// Send patch request
				Response response = httpRequest.patch();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}
	@Test(testName = "GetDepositoresidBranchesidfeeSchedulesfeebracets", priority = 14,enabled=false)
	public void getdepositoresidBranchesidfeeSchedulesfeebracetsTest() throws IOException 
	{
		RequestSpecification httpRequest=ClientPortal_RestAssuredRequest.getInstance();
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
        	
            if (keyword.equalsIgnoreCase("/api/depositories/{depositoryId}/branches/{depositoryBranchId}/fee-schedules/{depositoryFeeScheduleId}/fee-brackets(get)"))
            {	
            	String putpara1=xl.getCellData(xlfile, tcsheet, i, 4);
            	String putpara2=xl.getCellData(xlfile, tcsheet, i, 5);
            	String putpara3=xl.getCellData(xlfile, tcsheet, i, 6);
            	
            	int depositoryId=Integer.parseInt(putpara1);
            	int depositoryBranchId=Integer.parseInt(putpara2);
            	int depositoryFeeScheduleId=Integer.parseInt(putpara3);
            	httpRequest.basePath("/api/depositories/"+depositoryId+"/branches/"+depositoryBranchId+"/fee-schedules/"+depositoryFeeScheduleId+"/fee-brackets");

				// Create map of input parameters and set to request body if any!
            	Map<String, Object> params = new HashMap<String, Object>();
            	
            	params.put("depositoryId", depositoryId);
            	params.put("depositoryBranchId", depositoryBranchId);
            	params.put("depositoryFeeScheduleId", depositoryFeeScheduleId);
            	
            	
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));

				// Send get request
				Response response = httpRequest.get();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}
	@Test(testName = "PostDepositoresidBranchesidfeeSchedulesfeebracets", priority = 15,enabled=false)
	public void postdepositoresidBranchesidfeeSchedulesfeebracetsTest() throws IOException 
	{
		RequestSpecification httpRequest=ClientPortal_RestAssuredRequest.getInstance();
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
        	
            if (keyword.equalsIgnoreCase("/api/depositories/{depositoryId}/branches/{depositoryBranchId}/fee-schedules/{depositoryFeeScheduleId}/fee-brackets(post)"))
            {	
            	String putpara1=xl.getCellData(xlfile, tcsheet, i, 4);
            	String putpara2=xl.getCellData(xlfile, tcsheet, i, 5);
            	String putpara3=xl.getCellData(xlfile, tcsheet, i, 6);
            	String putpara4=xl.getCellData(xlfile, tcsheet, i, 7);
            	String putpara5=xl.getCellData(xlfile, tcsheet, i, 8);
            	String putpara6=xl.getCellData(xlfile, tcsheet, i, 9);
            	String putpara7=xl.getCellData(xlfile, tcsheet, i, 10);
            	int depositoryId=Integer.parseInt(putpara1);
            	int depositoryBranchId=Integer.parseInt(putpara2);
            	int depositoryFeeScheduleId=Integer.parseInt(putpara3);
            	httpRequest.basePath("/api/depositories/"+depositoryId+"/branches/"+depositoryBranchId+"/fee-schedules/"+depositoryFeeScheduleId+"/fee-brackets");

				// Create map of input parameters and set to request body if any!
            	Map<String, Object> params = new HashMap<String, Object>();
            	params.put("depositoryId", depositoryId);
            	params.put("depositoryBranchId", depositoryBranchId);
            	params.put("depositoryFeeScheduleId", depositoryFeeScheduleId);
            	params.put("name", putpara4);
            	params.put("minimum", putpara5);
            	params.put("maximum", putpara6);
            	params.put("fee", putpara7);
            	
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));

				// Send post request
				Response response = httpRequest.post();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}
	@Test(testName = "DeleteDepositoresidBranchesidfeeSchedulesfeebracets", priority = 16,enabled=false)
	public void deletedepositoresidBranchesidfeeSchedulesfeebracetsTest() throws IOException 
	{
		RequestSpecification httpRequest=ClientPortal_RestAssuredRequest.getInstance();
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
        	
            if (keyword.equalsIgnoreCase("/api/depositories/{depositoryId}/branches/{depositoryBranchId}/fee-schedules/{depositoryFeeScheduleId}/fee-brackets/{id}(delete)"))
            {	
            	String putpara1=xl.getCellData(xlfile, tcsheet, i, 4);
            	String putpara2=xl.getCellData(xlfile, tcsheet, i, 5);
            	String putpara3=xl.getCellData(xlfile, tcsheet, i, 6);
            	String putpara4=xl.getCellData(xlfile, tcsheet, i, 7);
            	int depositoryId=Integer.parseInt(putpara1);
            	int depositoryBranchId=Integer.parseInt(putpara2);
            	int depositoryFeeScheduleId=Integer.parseInt(putpara3);
            	int id=Integer.parseInt(putpara4);
            	httpRequest.basePath("/api/depositories/"+depositoryId+"/branches/"+depositoryBranchId+"/fee-schedules/"+depositoryFeeScheduleId+"/fee-brackets/"+id+"");

				// Create map of input parameters and set to request body if any!
            	Map<String, Object> params = new HashMap<String, Object>();
            	
            	params.put("depositoryId", depositoryId);
            	params.put("depositoryBranchId", depositoryBranchId);
            	params.put("depositoryFeeScheduleId", depositoryFeeScheduleId);
            	params.put("id", id);
            	
            	
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));

				// Send delete request
				Response response = httpRequest.delete();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}
	@Test(testName = "GetDepositoresidBranchesidfeeSchedulesfeebracetsid", priority = 17,enabled=false)
	public void getepositoresidBranchesidfeeSchedulesfeebracetsidTest() throws IOException 
	{
		RequestSpecification httpRequest=ClientPortal_RestAssuredRequest.getInstance();
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();
        
        for (int i = 0; i <=rc; i++) 
        {
        	
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
        	
            if (keyword.equalsIgnoreCase("/api/depositories/{depositoryId}/branches/{depositoryBranchId}/fee-schedules/{depositoryFeeScheduleId}/fee-brackets/{id}(get)"))
            {	
            	String putpara1=xl.getCellData(xlfile, tcsheet, i, 4);
            	String putpara2=xl.getCellData(xlfile, tcsheet, i, 5);
            	String putpara3=xl.getCellData(xlfile, tcsheet, i, 6);
            	String putpara4=xl.getCellData(xlfile, tcsheet, i, 7);
            	int depositoryId=Integer.parseInt(putpara1);
            	int depositoryBranchId=Integer.parseInt(putpara2);
            	int depositoryFeeScheduleId=Integer.parseInt(putpara3);
            	int id=Integer.parseInt(putpara4);
            	httpRequest.basePath("/api/depositories/"+depositoryId+"/branches/"+depositoryBranchId+"/fee-schedules/"+depositoryFeeScheduleId+"/fee-brackets/"+id+"");

				// Create map of input parameters and set to request body if any!
            	Map<String, Object> params = new HashMap<String, Object>();
            	
            	params.put("depositoryId", depositoryId);
            	params.put("depositoryBranchId", depositoryBranchId);
            	params.put("depositoryFeeScheduleId", depositoryFeeScheduleId);
            	params.put("id", id);
            	
            	
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));

				// Send get request
				Response response = httpRequest.get();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}
	@Test(testName = "PatchDepositoresidBranchesidfeeSchedulesfeebracetsid", priority = 18,enabled=false)
	public void patchdepositoresidBranchesidfeeSchedulesfeebracetsidTest() throws IOException 
	{
		RequestSpecification httpRequest=ClientPortal_RestAssuredRequest.getInstance();
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
        	
            if (keyword.equalsIgnoreCase("/api/depositories/{depositoryId}/branches/{depositoryBranchId}/fee-schedules/{depositoryFeeScheduleId}/fee-brackets/{id}(patch)"))
            {	
            	String putpara1=xl.getCellData(xlfile, tcsheet, i, 4);
            	String putpara2=xl.getCellData(xlfile, tcsheet, i, 5);
            	String putpara3=xl.getCellData(xlfile, tcsheet, i, 6);
            	String putpara4=xl.getCellData(xlfile, tcsheet, i, 7);
            	String putpara5=xl.getCellData(xlfile, tcsheet, i, 8);
            	String putpara6=xl.getCellData(xlfile, tcsheet, i, 9);
            	String putpara7=xl.getCellData(xlfile, tcsheet, i, 10);
            	String putpara8=xl.getCellData(xlfile, tcsheet, i, 11);
            	int depositoryId=Integer.parseInt(putpara1);
            	int depositoryBranchId=Integer.parseInt(putpara2);
            	int depositoryFeeScheduleId=Integer.parseInt(putpara3);
            	int id=Integer.parseInt(putpara4);
            	httpRequest.basePath("/api/depositories/"+depositoryId+"/branches/"+depositoryBranchId+"/fee-schedules/"+depositoryFeeScheduleId+"/fee-brackets/"+id+"");

				// Create map of input parameters and set to request body if any!
            	Map<String, Object> params = new HashMap<String, Object>();
            	params.put("depositoryId", depositoryId);
            	params.put("depositoryBranchId", depositoryBranchId);
            	params.put("depositoryFeeScheduleId", depositoryFeeScheduleId);
            	params.put("name", putpara5);
            	params.put("minimum", putpara6);
            	params.put("maximum", putpara7);
            	params.put("fee", putpara8);
            	
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));

				// Send patch request
				Response response = httpRequest.patch();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}
	
	@Test(testName = "POST_depositories_getclient", priority = 19,enabled=false)
	public void POST_depositories_getclient() throws IOException {
		
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();
				
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
	    int rc=ws.getLastRowNum();    
	    
	    for (int i = 0; i <=rc; i++) 
	    {
	    	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
	        
	        if (keyword.equalsIgnoreCase("post_/api/depositories/get/client"))
	        {	
	        	String putparam1=xl.getCellData(xlfile, tcsheet, i, 4);
	        	String putparam2=xl.getCellData(xlfile, tcsheet, i, 5);
	        	int intputparam2=Integer.parseInt(putparam2);
	        		        	       
	        	httpRequest.basePath("/api/depositories/get/client");
	        	
				// Create map of input parameters and set to request body if any!
				Map<String, Object> params = new HashMap<String, Object>();
				Map<String, Object> params1 = new HashMap<String, Object>();								
				params1.put("sponsorCode", putparam1);
				params1.put("accountNumber", intputparam2);					
														
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params1));
				
				// Send post request
				Response response = httpRequest.post();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
	    }
	}
	
	@Test(testName = "POST_depositories_client", priority = 20,enabled=false)
	public void POST_depositories_client() throws IOException {
		
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();
				
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
	    int rc=ws.getLastRowNum();    
	    
	    for (int i = 0; i <=rc; i++) 
	    {
	    	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
	        
	        if (keyword.equalsIgnoreCase("/api/depositories/client"))
	        {	
	        	String putparam1=xl.getCellData(xlfile, tcsheet, i, 4);
	        	String putparam2=xl.getCellData(xlfile, tcsheet, i, 5);
	        	String putparam3=xl.getCellData(xlfile, tcsheet, i, 6);
	        	int intputparam2=Integer.parseInt(putparam2);
	        	int inputparam3=Integer.parseInt(putparam3);	        	       
	        	httpRequest.basePath("/api/depositories/client");
	        	
				// Create map of input parameters and set to request body if any!
				Map<String, Object> params = new HashMap<String, Object>();
				Map<String, Object> params1 = new HashMap<String, Object>();								
				params1.put("sponsorCode", putparam1);
				params1.put("accountNumber", intputparam2);	
				params1.put("depositoryFeeScheduleId", inputparam3);	
														
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params1));
				
				// Send post request
				Response response = httpRequest.post();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
	    }
	}
	@Test(testName = "DELETE_depositories_deleteclient", priority = 21,enabled=false)
	public void DELETE_depositories_deleteclient() throws IOException {
		
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();
				
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
	    int rc=ws.getLastRowNum();    
	    
	    for (int i = 0; i <=rc; i++) 
	    {
	    	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
	        
	        if (keyword.equalsIgnoreCase("/api/depositories/delete/client"))
	        {	
	        	String putparam1=xl.getCellData(xlfile, tcsheet, i, 4);
	        	String putparam2=xl.getCellData(xlfile, tcsheet, i, 5);
	        	int intputparam2=Integer.parseInt(putparam2);
	        		        	       
	        	httpRequest.basePath("/api/depositories/delete/client");
	        	
				// Create map of input parameters and set to request body if any!
				Map<String, Object> params = new HashMap<String, Object>();
				Map<String, Object> params1 = new HashMap<String, Object>();								
				params1.put("sponsorCode", putparam1);
				params1.put("accountNumber", intputparam2);					
														
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params1));
				
				// Send post request
				Response response = httpRequest.delete();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
	    }
	}
	@Test(testName = "DELETE_depositories_deleteclient", priority = 22,enabled=false)
	public void DELETE_frontend_sponsor_depositoriesid_delete() throws IOException {
		
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();
				
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
	    int rc=ws.getLastRowNum();    
	    
	    for (int i = 0; i <=rc; i++) 
	    {
	    	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
	        
	        if (keyword.equalsIgnoreCase("/api/frontend/sponsors/{sponsorCode}/depositories/{id}"))
	        {	
	        	String putparam1=xl.getCellData(xlfile, tcsheet, i, 4);
	        	String putparam2=xl.getCellData(xlfile, tcsheet, i, 5);
	        	int intputparam2=Integer.parseInt(putparam2);
	        		        	       
	        	httpRequest.basePath("/api/frontend/sponsors/"+putparam1+"/depositories/"+intputparam2+"");
	        	
				// Create map of input parameters and set to request body if any!
				Map<String, Object> params = new HashMap<String, Object>();
				Map<String, Object> params1 = new HashMap<String, Object>();								
				params1.put("sponsorCode", putparam1);
				params1.put("id", intputparam2);					
														
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params1));
				
				// Send delete request
				Response response = httpRequest.delete();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
	    }
	}
	@Test(testName = "GET_Sponssors_Depositories", priority = 23,enabled=false)
	public void GET_Sponssors_DepositoriesTest() throws IOException {
		
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();
				
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
	    int rc=ws.getLastRowNum();    
	    
	    for (int i = 0; i <=rc; i++) 
	    {
	    	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
	        
	        if (keyword.equalsIgnoreCase("/api/frontend/sponsors/{sponsorCode}/depositories/{id}"))
	        {	
	        	String putparam1=xl.getCellData(xlfile, tcsheet, i, 4);
	        	
	        	httpRequest.basePath("/api/sponsors/"+putparam1+"/depositories");
	        	
				// Create map of input parameters and set to request body if any!
				Map<String, Object> params = new HashMap<String, Object>();
												
				params.put("sponsorCode", putparam1);
					
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));
				
				// Send get request
				Response response = httpRequest.get();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
	    }
	}
	@Test(testName = "DELETE_Sponssors_Depositoriesid", priority = 24,enabled=false)
	public void DELETE_Sponssors_DepositoriesidTest() throws IOException {
		
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();
				
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
	    int rc=ws.getLastRowNum();    
	    
	    for (int i = 0; i <=rc; i++) 
	    {
	    	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
	        
	        if (keyword.equalsIgnoreCase("/api/sponsors/{sponsorCode}/depositories/{id}"))
	        {	
	        	String putparam1=xl.getCellData(xlfile, tcsheet, i, 4);
	        	String putparam2=xl.getCellData(xlfile, tcsheet, i, 5);
	        	int intputparam2=Integer.parseInt(putparam2);
	        	
	        	httpRequest.basePath("/api/sponsors/"+putparam1+"/depositories/"+intputparam2+"");
	        	
				// Create map of input parameters and set to request body if any!
				Map<String, Object> params = new HashMap<String, Object>();
												
				params.put("sponsorCode", putparam1);
				params.put("id",intputparam2);	
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));
				
				// Send delete request
				Response response = httpRequest.delete();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
	    }
	}
	@Test(testName = "GET_Sponsor_Depositories", priority = 25,enabled=false)
	public void GET_Sponsor_DepositoriesTest() throws IOException {
		
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();
		
	    httpRequest.basePath("/api/sponsors/depositories");
	        	
				// Create map of input parameters and set to request body if any!
				Map<String, Object> params = new HashMap<String, Object>();
				
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));
				
				// Send get request
				Response response = httpRequest.get();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
	    }
	

