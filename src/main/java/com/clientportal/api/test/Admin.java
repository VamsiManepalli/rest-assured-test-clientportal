package com.clientportal.api.test;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.runners.Suite.SuiteClasses;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.clientportal.api.listeners.ClientPortal_TestsListener;
import com.clientportal.api.test.beans.AuthResponseBody;
import com.clientportal.api.test.beans.ClientPortal_UserProfileBean;
import com.clientportal.api.test.utils.ClientPortal_RestAssuredRequest;

import com.clientportal.api.test.utils.ClientPortal_Utils;
import com.clientportal.api.test.utils.ClientPortal_XLUtils;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

@SuiteClasses(value = { authenticate.class })
@Listeners(value = ClientPortal_TestsListener.class)

public class Admin
{

	//Xlutils
	ClientPortal_XLUtils xl=new ClientPortal_XLUtils();	
	String xlfile=ClientPortal_Utils.getProperty("file.xlfile.path");
	String tcsheet = "TestData";
	
	
	ObjectMapper mapper = null;
	Map<String, String> cookies = null;
	ClientPortal_UserProfileBean results = null;
	private static String usersignuptoken;
		
	@BeforeClass
	public void loadResource() {
		mapper = new ObjectMapper();
	}
	@Test(testName = "AdmingetAdmin", priority = 0,enabled=false)
	public void admingetAdmin() throws IOException 
	{
		RequestSpecification httpRequest=ClientPortal_RestAssuredRequest.getInstance();
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
        	
            if (keyword.equalsIgnoreCase("/admin"))
            {	
            	
            	httpRequest.basePath("/admin");

				// Create map of input parameters and set to request body if any!
            	Map<String, Object> params = new HashMap<String, Object>();
								
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));

				// Send post request
				Response response = httpRequest.get();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}
	@Test(testName = "AdminRoles", priority = 1,enabled=false)
	public void adminRoles() throws IOException 
	{
		RequestSpecification httpRequest=ClientPortal_RestAssuredRequest.getInstance();
		
            	httpRequest.basePath("/admin/roles");
				// Send get request
				Response response = httpRequest.get();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				// Validate status code
				response.then().assertThat().statusCode(200);
			
	}
	@Test(testName = "AdminSponsors", priority = 2,enabled=false)
	public void adminSponsors() throws IOException 
	{
		RequestSpecification httpRequest=ClientPortal_RestAssuredRequest.getInstance();
		
            	httpRequest.basePath("/admin/sponsors");
				// Send get request
				Response response = httpRequest.get();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
	
	@Test(testName = "AdminActivate", priority = 3,enabled=false)
	public void adminActivate() throws IOException 
	{
		RequestSpecification httpRequest=ClientPortal_RestAssuredRequest.getInstance();
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
        	
            if (keyword.equalsIgnoreCase("/admin/sponsors/{sponsorCode}/activate"))
            {	
            	String putpara1=xl.getCellData(xlfile,tcsheet , i, 4);
            	//System.out.println(putpara1);
            	httpRequest.basePath("/admin/sponsors/"+putpara1+"/activates");

				// Create map of input parameters and set to request body if any!
            	Map<String, Object> params = new HashMap<String, Object>();
            	params.put("sponsorCode", putpara1);
            	
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));

				// Send post request
				Response response = httpRequest.get();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}
	@Test(testName = "AdminSponsorsResetPassword", priority = 4,enabled=false)
	public void adminSponsorsResetPassword() throws IOException 
	{
		RequestSpecification httpRequest=ClientPortal_RestAssuredRequest.getInstance();
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
        	
            if (keyword.equalsIgnoreCase("/admin/sponsors/{sponsorCode}/reset-password"))
            {	
            	String putpara1=xl.getCellData(xlfile,tcsheet , i, 4);
            	String putpara2=xl.getCellData(xlfile,tcsheet , i, 5);
            	//System.out.println(putpara1);
            	httpRequest.basePath("/admin/sponsors/"+putpara1+"/reset-password");

				// Create map of input parameters and set to request body if any!
            	Map<String, Object> params = new HashMap<String, Object>();
            	Map<String, Object> params1 = new HashMap<String, Object>();
            	params.put("sponsorCode", putpara1);
				params1.put("password", putpara2);
				params.put("Body", mapper.writeValueAsString(params1));
				
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));
				
				// Send post request
				Response response = httpRequest.post();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}
	@Test(testName = "AdminDeleteAccounts", priority = 5,enabled=false)
	public void adminDeleteAcconts() throws IOException 
	{
		RequestSpecification httpRequest=ClientPortal_RestAssuredRequest.getInstance();
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
        	
            if (keyword.equalsIgnoreCase("/api/admin/accounts"))
            {	
            	String putpara1=xl.getCellData(xlfile,tcsheet , i, 4);
            	
            	//System.out.println(putpara1);
            	httpRequest.basePath("/api/admin/accounts");

				// Create map of input parameters and set to request body if any!
            	Map<String, Object> params = new HashMap<String, Object>();
            	params.put("accountNumber", putpara1);
            	
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));

				// Send post request
				Response response = httpRequest.delete();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}
	@Test(testName = "AdminClientUserAllocations", priority = 6,enabled=false)
	public void adminClientUserAllocations() throws IOException 
	{
		RequestSpecification httpRequest=ClientPortal_RestAssuredRequest.getInstance();
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
        	String putpara1=xl.getCellData(xlfile,tcsheet , i, 4);
        	String putpara2=xl.getCellData(xlfile,tcsheet , i, 5);
            if (keyword.equalsIgnoreCase("/api/v1/admin/client-users/{accountNumber}/allocations"))
            {	
            	//System.out.println(putpara1);
            	httpRequest.basePath("/api/v1/admin/client-users/"+putpara1+"/allocations");

				// Create map of input parameters and set to request body if any!
            	Map<String, Object> params = new HashMap<String, Object>();
            	params.put("accountNumber", putpara1);
            	params.put("apiKey", putpara2);
            	
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));
				System.out.println(params);

				// Send get request
				Response response = httpRequest.get();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}
	@Test(testName = "AdminTermsandConditions", priority = 7,enabled=false)
	public void adminTermsandConditions() throws IOException 
	{
		RequestSpecification httpRequest=ClientPortal_RestAssuredRequest.getInstance();
		
            	httpRequest.basePath("/admin/terms-and-conditions");
				// Send get request
				Response response = httpRequest.get();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				// Validate status code
				response.then().assertThat().statusCode(200);
			
	}
	@Test(testName = "AddAllSponsors", priority = 8,enabled=false)
	public void addAllSponsors() throws IOException 
	{
		RequestSpecification httpRequest=ClientPortal_RestAssuredRequest.getInstance();
		
            	httpRequest.basePath("/admin/sponsors/add-all-sponsors");
            	
				// Send get request
				Response response = httpRequest.get();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				
				// Validate status code
				response.then().assertThat().statusCode(200);
			
	}

	@Test(testName = "AdminTermsandConditionsTest", priority = 9,enabled=false)
	public void adminTermsandConditionsTest() throws IOException 
	{
		RequestSpecification httpRequest=ClientPortal_RestAssuredRequest.getInstance();
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
        	String putpara1=xl.getCellData(xlfile,tcsheet , i, 4);
        	String putpara2=xl.getCellData(xlfile,tcsheet , i, 5);
            if (keyword.equalsIgnoreCase("/admin/terms-and-conditions-test"))
            {	
            	//System.out.println(putpara1);
            	httpRequest.basePath("/admin/terms-and-conditions-test");

				// Create map of input parameters and set to request body if any!
            	Map<String, Object> params = new HashMap<String, Object>();
            	params.put("termsAndConditionsName", putpara1);
            	params.put("sponsorCode", putpara2);
            	
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));
				System.out.println(params);

				// Send get request
				Response response = httpRequest.get();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}
	@Test(testName = "deleteAdminTermsandConditionsid", priority = 10,enabled=false)
	public void deleteAdminTermsandConditionsid() throws IOException 
	{
		RequestSpecification httpRequest=ClientPortal_RestAssuredRequest.getInstance();
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
        	String putpara1=xl.getCellData(xlfile,tcsheet , i, 4);
        	
            if (keyword.equalsIgnoreCase("/admin/terms-and-conditions/{id}"))
            {	
            	//System.out.println(putpara1);
            	httpRequest.basePath("/admin/terms-and-conditions/"+putpara1+"");

				// Create map of input parameters and set to request body if any!
            	Map<String, Object> params = new HashMap<String, Object>();
            	params.put("id", putpara1);
            	
            	
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));
				
				// Send get request
				Response response = httpRequest.delete();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}
	@Test(testName = "patchAdminTermsandConditionsid", priority = 11,enabled=false)
	public void patchAdminTermsandConditionsid() throws IOException 
	{
		RequestSpecification httpRequest=ClientPortal_RestAssuredRequest.getInstance();
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
        	String putpara1=xl.getCellData(xlfile,tcsheet , i, 4);
        	
        	String putpara2=xl.getCellData(xlfile,tcsheet , i, 5);
        	
        	String putpara3=xl.getCellData(xlfile,tcsheet , i, 6);
        	
        	String putpara4=xl.getCellData(xlfile,tcsheet , i, 7);
        	
        	String putpara5=xl.getCellData(xlfile,tcsheet , i, 8);
        	
        	String putpara6=xl.getCellData(xlfile,tcsheet , i, 9);
        	
        	String putpara7=xl.getCellData(xlfile,tcsheet , i, 10);
        	String putpara8=xl.getCellData(xlfile,tcsheet , i, 11);
        	
        	
            if (keyword.equalsIgnoreCase("/admin/terms-and-conditions1/{id}"))
            {	
            	//System.out.println(putpara1);
            	httpRequest.basePath("/admin/terms-and-conditions/"+putpara1+"");

				// Create map of input parameters and set to request body if any!
            	Map<String, Object> params = new HashMap<String, Object>();
            	Map<String, Object> params1 = new HashMap<String, Object>();
            	params.put("id", putpara1);
            	params1.put("name", putpara2);
            	params1.put("version", Integer.parseInt(putpara3));
            	params1.put("isActive", true);
            	params1.put("description", putpara5);
            	params1.put("text", putpara6);
            	params1.put("html", Integer.parseInt(putpara7));
            	params1.put("activatedAt", putpara8);
            	params.put("Body", params1);
            	System.out.println(params);
            	
            	
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));
				
				// Send get request
				Response response = httpRequest.patch();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}
	
}
        
	
