package com.clientportal.api.test;

import static org.testng.Assert.assertEquals;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.sl.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.runners.Suite.SuiteClasses;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.clientportal.api.listeners.ClientPortal_TestsListener;
import com.clientportal.api.test.beans.AuthResponseBody;
import com.clientportal.api.test.beans.ClientPortal_RequestBody;
import com.clientportal.api.test.beans.ClientPortal_UserProfileBean;
import com.clientportal.api.test.beans.ClientPortal_XLReportBean;
import com.clientportal.api.test.utils.ClientPortal_RestAssuredRequest;
import com.clientportal.api.test.utils.ClientPortal_Utils;
import com.clientportal.api.test.utils.ClientPortal_XLRepostGenerator;
import com.clientportal.api.test.utils.ClientPortal_XLUtils;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.restassured.matcher.ResponseAwareMatcher;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

@SuiteClasses(value = { authenticate.class })
@Listeners(value = ClientPortal_TestsListener.class)
public class authenticate {
	
	//Xlutils
	ClientPortal_XLUtils xl=new ClientPortal_XLUtils();	
	String xlfile=ClientPortal_Utils.getProperty("file.xlfile.path");
	String tcsheet = "TestData";
	
	
	ObjectMapper mapper = null;
	Map<String, String> cookies = null;
	ClientPortal_UserProfileBean results = null;
	private static String authToken;

	@BeforeClass
	public void loadResource() {
		mapper = new ObjectMapper();
	}

	@Test(testName = "client-user", priority = 0)
	public void clientuserTest() throws IOException {
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();
		httpRequest.basePath("/api/login/client-user");
		//Map<String, Object> params = ClientPortal_Utils.getPropertyHashMap("authenticationTest.request.params");
		
		// Create map of input parameters and set to request body if any!
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("username", "happy@client.com");
		params.put("password", "Happyclient1!");


		// Convert Java Object to JSON string
		httpRequest.body(mapper.writeValueAsString(params));

		// Send post request
		Response response = httpRequest.post();
		ResponseBody body = response.getBody();
		System.out.println(response.getBody().asString());
		authToken = mapper.readValue(response.getBody().asString(), AuthResponseBody.class).getData().getAuthenticationToken();
		//sSystem.out.println(authToken);
		// Validate status code
		response.then().assertThat().statusCode(200);
		
		/*if(isValidCredentials) {
			response.then().assertThat().body("result.success", equalTo(true));
			cookies = response.getCookies();
			httpRequest.cookies(cookies);
		} else {
			response.then().assertThat().body("error.code", equalTo(401)).and()
			.body("error.message", equalTo("not authorized for specified method"));
		}*/
	}
	@Test(testName = "client-user-admin", priority = 1)
	public void clientuseradminTest() throws IOException {
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();
		httpRequest.basePath("/api/login/admin-user");
		//Map<String, Object> params = ClientPortal_Utils.getPropertyHashMap("authenticationTest.request.params");
		
		// Create map of input parameters and set to request body if any!
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("username", "techsupport");
		params.put("password", "Grat3ful!");


		// Convert Java Object to JSON string
		httpRequest.body(mapper.writeValueAsString(params));

		// Send post request
		Response response = httpRequest.post();
		ResponseBody body = response.getBody();
		System.out.println(response.getBody().asString());
		authToken = mapper.readValue(response.getBody().asString(), AuthResponseBody.class).getData().getAuthenticationToken();
		// Validate status code
		response.then().assertThat().statusCode(200);
		
		/*if(isValidCredentials) {
			response.then().assertThat().body("result.success", equalTo(true));
			cookies = response.getCookies();
			httpRequest.cookies(cookies);
		} else {
			response.then().assertThat().body("error.code", equalTo(401)).and()
			.body("error.message", equalTo("not authorized for specified method"));
		}*/
	}
	
}
