package com.clientportal.api.test.beans;

import java.io.Serializable;
import java.util.Date;

public class ClientPortal_XLReportBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7898328930251533274L;

	private String testName;
	private String status;
	private String errorMessage;
	private Date startDate;
	private Date endDate;

	public String getTestName() {
		return testName;
	}

	public void setTestName(String testName) {
		this.testName = testName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String string) {
		this.status = string;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

}
