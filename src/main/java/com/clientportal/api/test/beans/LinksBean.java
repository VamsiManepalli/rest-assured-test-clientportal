package com.clientportal.api.test.beans;

import java.io.Serializable;

public class LinksBean  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7898632290669692316L;
	
	private String self;

	public String getSelf() {
		return self;
	}

	public void setSelf(String self) {
		this.self = self;
	}
}
