package com.clientportal.api.test.beans;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ClientPortal_UserProfileBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 939806366998585506L;

	private int code;
	private int count;
	private String offset;
	private String limit;
	private String hasMore;
	private String dateTimeUtc;
	private String auditTrailId;
	private String self;
	private String authenticationToken;
	

	public int getId() {
		return code;
	}

	public void setcode(int code) {
		this.code = code;
	}

	public int getcount() {
		return count;
	}

	public void setcount(int count) {
		this.count = count;
	}

	public String getoffset() {
		return offset;
	}

	public void setoffset(String offset) {
		this.offset = offset;
	}

	public String getlimit() {
		return limit;
	}

	public void setlimit(String limit) {
		this.limit = limit;
	}

	public String gethasMore() {
		return hasMore;
	}

	public void sethasMore(String hasMore) {
		this.hasMore = hasMore;
	}

	public String getdateTimeUtc() {
		return dateTimeUtc;
	}

	public void setdateTimeUtc(String dateTimeUtc) {
		this.dateTimeUtc = dateTimeUtc;
	}

	public String getauditTrailId() {
		return auditTrailId;
	}

	public void setauditTrailId(String auditTrailId) {
		this.auditTrailId = auditTrailId;
	}
	
	public String getself() {
		return self;
	}

	public void setself(String self) {
		this.self = self;
	}
	
	public String getauthenticationToken() {
		return authenticationToken;
	}

	public void setauthenticationToken(String authenticationToken) {
		this.authenticationToken = authenticationToken;
	}

	
}
