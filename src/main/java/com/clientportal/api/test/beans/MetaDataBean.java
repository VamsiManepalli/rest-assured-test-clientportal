package com.clientportal.api.test.beans;

import java.io.Serializable;

public class MetaDataBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -565526466795172726L;

	private int code;
	private int count;
	private String offset;
	private String limit;
	private String hasMore;
	private String dateTimeUtc;
	private String auditTrailId;

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getOffset() {
		return offset;
	}

	public void setOffset(String offset) {
		this.offset = offset;
	}

	public String getLimit() {
		return limit;
	}

	public void setLimit(String limit) {
		this.limit = limit;
	}

	public String getHasMore() {
		return hasMore;
	}

	public void setHasMore(String hasMore) {
		this.hasMore = hasMore;
	}

	public String getDateTimeUtc() {
		return dateTimeUtc;
	}

	public void setDateTimeUtc(String dateTimeUtc) {
		this.dateTimeUtc = dateTimeUtc;
	}

	public String getAuditTrailId() {
		return auditTrailId;
	}

	public void setAuditTrailId(String auditTrailId) {
		this.auditTrailId = auditTrailId;
	}
}
