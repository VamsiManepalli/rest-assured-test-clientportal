package com.clientportal.api.test.beans;

import java.io.Serializable;

public class DataBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6995484324231244820L;
	private String authenticationToken;
	private String emailAddress;
	private String firstName;
	private String middleName;
	private String lastName;
	private String status;
	private String userSignupToken;


	public String getAuthenticationToken() {
		return authenticationToken;
	}

	public void setAuthenticationToken(String authenticationToken)
	{
		
		this.authenticationToken = authenticationToken;
	}
	
	public String getemailAddress() {
		return emailAddress;
	}

	public void setemailAddress(String emailAddress)
	{
		
		this.emailAddress = emailAddress;
	}
	public String getfirstName() {
		return firstName;
	}

	public void setfirstName(String firstName)
	{
		
		this.firstName = firstName;
	}
	public String getmiddleName() {
		return middleName;
	}

	public void setmiddleName(String middleName)
	{
		
		this.middleName = middleName;
	}
	public String getlastName() {
		return lastName;
	}

	public void setlastName(String lastName)
	{
		
		this.lastName= lastName;
	}
	public String getstatus() {
		return status;
	}

	public void setstatus(String status)
	{
		
		this.status= status;
	}
	public String getuserSignupToken() {
		return userSignupToken;
	}

	public void setuserSignupToken(String userSignupToken)
	{
		
		this.userSignupToken= userSignupToken;
	}
}
