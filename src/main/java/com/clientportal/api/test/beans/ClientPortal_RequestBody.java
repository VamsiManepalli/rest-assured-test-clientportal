package com.clientportal.api.test.beans;

import java.io.Serializable;
import java.util.Map;

public class ClientPortal_RequestBody implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4642545410713193047L;
	
	private Map<String,Object> params;
	
	public Map<String, Object> getParams() {
		return params;
	}
	public void setParams(Map<String, Object> params) {
		this.params = params;
	}
	
	public String toJSON() {
		StringBuilder json = new StringBuilder("{");
		json.append(params);
		return json.toString();
	}
}
