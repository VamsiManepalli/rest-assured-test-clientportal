package com.clientportal.api.test.beans;

import java.io.Serializable;

public class AuthResponseBody implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7826271995799726858L;

	private MetaDataBean meta;
	private LinksBean links;
	private DataBean data;

	public MetaDataBean getMeta() {
		return meta;
	}

	public void setMeta(MetaDataBean meta) {
		this.meta = meta;
	}

	public LinksBean getLinks() {
		return links;
	}

	public void setLinks(LinksBean links) {
		this.links = links;
	}

	public DataBean getData() {
		return data;
	}

	public void setData(DataBean data) {
		this.data = data;
	}
}
