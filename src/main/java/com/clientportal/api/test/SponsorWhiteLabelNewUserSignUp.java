package com.clientportal.api.test;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.runners.Suite.SuiteClasses;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.clientportal.api.listeners.ClientPortal_TestsListener;
import com.clientportal.api.test.beans.AuthResponseBody;
import com.clientportal.api.test.beans.ClientPortal_UserProfileBean;
import com.clientportal.api.test.utils.ClientPortal_RestAssuredRequest;
import com.clientportal.api.test.utils.ClientPortal_RestAssuredRequest_Sponsor;
import com.clientportal.api.test.utils.ClientPortal_Utils;
import com.clientportal.api.test.utils.ClientPortal_XLUtils;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;
@SuiteClasses(value = { authenticate.class })
@Listeners(value = ClientPortal_TestsListener.class)

public class SponsorWhiteLabelNewUserSignUp
{

	//Xlutils
	ClientPortal_XLUtils xl=new ClientPortal_XLUtils();	
	String xlfile=ClientPortal_Utils.getProperty("file.xlfile.path");
	String tcsheet = "TestData";
	
	
	ObjectMapper mapper = null;
	Map<String, String> cookies = null;
	ClientPortal_UserProfileBean results = null;
	private static String usersignuptoken;
		
	@BeforeClass
	public void loadResource() {
		mapper = new ObjectMapper();
	}
	@Test(testName = "SponsorWhiteLabelNewUserSignUp-Sponsors", priority = 0,enabled=false)
	public void SponsorWhiteLabelNewUserSignUpSponsors() throws IOException 
	{
		RequestSpecification httpRequest=ClientPortal_RestAssuredRequest_Sponsor.getInstance();
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
        	
            if (keyword.equalsIgnoreCase("/api/v2/sponsors"))
            {	
            	
            	httpRequest.basePath("/api/v2/sponsors");

				// Create map of input parameters and set to request body if any!
            	Map<String, Object> params = new HashMap<String, Object>();
								
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));

				// Send post request
				Response response = httpRequest.get();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}
	@Test(testName = "SponsorWhiteLabelNewUserSignUp-Sponsors-ClientCoupons", priority = 1,enabled=false)
	public void SponsorWhiteLabelNewUserSignUpSponsorsClientCoupons() throws IOException 
	{
		RequestSpecification httpRequest=ClientPortal_RestAssuredRequest_Sponsor.getInstance();
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
        	
            if (keyword.equalsIgnoreCase("/api/v2/sponsors/clients/coupons/validate"))
            {	
            	String putpara1=xl.getCellData(xlfile, tcsheet, i, 4);
            	httpRequest.basePath("/api/v2/sponsors/clients/coupons/validate?couponCode="+putpara1+"");
            	httpRequest.queryParam("couponCode",putpara1 );

				// Create map of input parameters and set to request body if any!
            	
            	Map<String, Object> params = new HashMap<String, Object>();
            	
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));

				// Send post request
				Response response = httpRequest.get();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}
	@Test(testName = "SponsorWhiteLabelNewUserSignUp-Step1", priority = 2)
	public void sponsorWhiteLabelNewUserSignUpStep1() throws IOException 
	{
		RequestSpecification httpRequest=ClientPortal_RestAssuredRequest_Sponsor.getInstance();
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
        	
            if (keyword.equalsIgnoreCase("/api/v2/sponsors/clients/sign-up"))
            {	
            	String putpara1=xl.getCellData(xlfile, tcsheet, i, 4);
            	String putpara2=xl.getCellData(xlfile, tcsheet, i, 5);
            	String putpara3=xl.getCellData(xlfile, tcsheet, i, 6);
            	String putpara4=xl.getCellData(xlfile, tcsheet, i, 7);
            	String putpara5=xl.getCellData(xlfile, tcsheet, i, 8);
            	
            	httpRequest.basePath("/api/v2/sponsors/clients/sign-up");

				// Create map of input parameters and set to request body if any!
            	
            	Map<String, Object> params = new HashMap<String, Object>();
            	params.put("emailAddress", putpara1);
            	params.put("firstName", putpara2);
            	params.put("middleName", putpara3);
            	params.put("lastName", putpara4);
            	params.put("password", putpara5);
            	params.put("SponsorClientBasic", mapper.writeValueAsString(params));
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));
				

				// Send post request
				Response response = httpRequest.post();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				usersignuptoken = mapper.readValue(response.getBody().asString(), AuthResponseBody.class).getData().getuserSignupToken();
				//System.out.println(usersignuptoken);
				
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}
	@Test(testName = "SponsorWhiteLabelNewUserSignUp-Step2", priority = 3,enabled=false)
	public void sponsorWhiteLabelNewUserSignUpStep2() throws IOException 
	{
		RequestSpecification httpRequest=ClientPortal_RestAssuredRequest_Sponsor.getInstance();
		
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
        	
            if (keyword.equalsIgnoreCase("/api/v2/sponsors/clients/sign-up2"))
            {	
            	String putpara1=xl.getCellData(xlfile, tcsheet, i, 4);
            	String putpara2=xl.getCellData(xlfile, tcsheet, i, 5);
            	String putpara3=xl.getCellData(xlfile, tcsheet, i, 6);
            	String putpara4=xl.getCellData(xlfile, tcsheet, i, 7);
            	String putpara5=xl.getCellData(xlfile, tcsheet, i, 8);
            	String putpara6=xl.getCellData(xlfile, tcsheet, i, 9);
            	String putpara7=xl.getCellData(xlfile, tcsheet, i, 10);
            	String putpara8=xl.getCellData(xlfile, tcsheet, i, 11);
            	String putpara9=xl.getCellData(xlfile, tcsheet, i, 12);
            	String putpara10=xl.getCellData(xlfile, tcsheet, i, 13);
            	String putpara11=xl.getCellData(xlfile, tcsheet, i, 14);
            	String putpara12=xl.getCellData(xlfile, tcsheet, i, 15);
            	String putpara13=xl.getCellData(xlfile, tcsheet, i, 16);
            	String putpara14=xl.getCellData(xlfile, tcsheet, i, 17);
            	String putpara15=xl.getCellData(xlfile, tcsheet, i, 18);
            	String putpara16=xl.getCellData(xlfile, tcsheet, i, 19);
            	String putpara17=xl.getCellData(xlfile, tcsheet, i, 20);
            	String putpara18=xl.getCellData(xlfile, tcsheet, i, 21);
            	String putpara19=xl.getCellData(xlfile, tcsheet, i, 22);
            	String putpara20=xl.getCellData(xlfile, tcsheet, i, 23);
            	String putpara21=xl.getCellData(xlfile, tcsheet, i, 24);
            	String putpara22=xl.getCellData(xlfile, tcsheet, i, 25);
            	String putpara23=xl.getCellData(xlfile, tcsheet, i, 26);
            	String putpara24=xl.getCellData(xlfile, tcsheet, i, 27);
            	
            	httpRequest.basePath("/api/v2/sponsors/clients/sign-up");

				// Create map of input parameters and set to request body if any!
            	
            	Map<String, Object> params = new HashMap<String, Object>();
            	
            	//Map<String, Object> params2 = new HashMap<String, Object>();
            	
            	httpRequest.header("userSignupToken",usersignuptoken);
            	//params.put("userSignupToken", usersignuptoken);
                params.put("dateOfBirth", putpara1);
            	params.put("SSN",putpara2);
            	params.put("maritalStatusId", 198);
            	params.put("phone", putpara4);
            	params.put("streetAddress1", putpara5);
            	params.put("streetCity",putpara6);
            	params.put("streetState", putpara7);
            	params.put("streetZipCode", putpara8);
            	params.put("mailingAddress1", putpara9);
            	params.put("mailingCity", putpara10);
            	params.put("mailingState", putpara11);
            	params.put("mailingZipCode",putpara12);
            	params.put("billingAddress1", putpara13);
            	params.put("billingCity", putpara14);
            	params.put("billingState", putpara15);
            	params.put("billingZipCode", putpara16);
            	params.put("emailNotification", true);
            	params.put("annualFeeMethodId", 585);
            	params.put("feeScheduleId", 20);
            	params.put("termsAgreed", true);
            	params.put("feeScheduleAgreed", true);
            	params.put("accountTypeId", 1);
            	params.put("transactionFeeMethod", 604);
            	
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));

				// Send post request
				Response response = httpRequest.patch();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				
				
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}
	@Test(testName = "sponsorWhiteLabelNewUserSignUpStorageOptions", priority = 4,enabled=false)
	public void sponsorWhiteLabelNewUserSignUpStorageOptions() throws IOException 
	{
		RequestSpecification httpRequest=ClientPortal_RestAssuredRequest_Sponsor.getInstance();
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
        	
            if (keyword.equalsIgnoreCase("/api/v2/sponsors/clients/sign-up/storage-options"))
            {	
            	String putpara1=xl.getCellData(xlfile, tcsheet, i, 4);
            	
            	httpRequest.basePath("/api/v2/sponsors/clients/sign-up/storage-options");

				// Create map of input parameters and set to request body if any!
            	
            	Map<String, Object> params = new HashMap<String, Object>();
            	Map<String, Object> params1 = new HashMap<String, Object>();
            	
            	//Map<String, Object> params2 = new HashMap<String, Object>();
            	
            	httpRequest.header("userSignupToken",usersignuptoken);
            	params.put("depository", Integer.parseInt(putpara1));
            	
				// Convert Java Object to JSON string
            	
				httpRequest.body(mapper.writeValueAsString(params));

				// Send post request
				Response response = httpRequest.post();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}
	@Test(testName = "sponsorWhiteLabelNewUserSignUpFinalize", priority = 5)
	public void sponsorWhiteLabelNewUserSignUpFinalize() throws IOException 
	{
		RequestSpecification httpRequest=ClientPortal_RestAssuredRequest_Sponsor.getInstance();
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
        	
            if (keyword.equalsIgnoreCase("/api/v2/sponsors/clients/sign-up/finalize"))
            {	
            	String putpara1=xl.getCellData(xlfile, tcsheet, i, 4);
            	String putpara2=xl.getCellData(xlfile, tcsheet, i, 5);
            	String putpara3=xl.getCellData(xlfile, tcsheet, i, 6);
            	String putpara4=xl.getCellData(xlfile, tcsheet, i, 7);
            	String putpara5=xl.getCellData(xlfile, tcsheet, i, 8);
            	
            	httpRequest.basePath("/api/v2/sponsors/clients/sign-up/finalize");

				// Create map of input parameters and set to request body if any!
            	
            	Map<String, Object> params = new HashMap<String, Object>();
            	
            	httpRequest.header("userSignupToken",usersignuptoken);
            	httpRequest.header("couponCode",putpara1);
            	params.put("cardHolderName", putpara2);
            	params.put("cardNumber", putpara3);
            	params.put("expiryDate", putpara4);
            	params.put("cvv", putpara5);
            	
				// Convert Java Object to JSON string
            	
				httpRequest.body(mapper.writeValueAsString(params));

				// Send post request
				Response response = httpRequest.post();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}
	@Test(testName = "SponsorWhiteLabelNewUserSignUp-clients-status", priority = 6,enabled=false)
	public void sponsorWhiteLabelNewUserSignUpclientsstatus() throws IOException 
	{
		RequestSpecification httpRequest=ClientPortal_RestAssuredRequest_Sponsor.getInstance();
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
        	
            if (keyword.equalsIgnoreCase("/api/v2/sponsors/clients/status/{account}"))
            {	
            	String putpara1=xl.getCellData(xlfile, tcsheet, i, 4);
            	httpRequest.basePath("/api/v2/sponsors/clients/status/"+putpara1+"");

				// Create map of input parameters and set to request body if any!
            	
            	Map<String, Object> params = new HashMap<String, Object>();
            	params.put("account", putpara1);
								
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));

				// Send post request
				Response response = httpRequest.get();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}
}
