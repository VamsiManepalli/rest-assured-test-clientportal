package com.clientportal.api.test;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.runners.Suite.SuiteClasses;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.clientportal.api.listeners.ClientPortal_TestsListener;
import com.clientportal.api.test.beans.ClientPortal_UserProfileBean;
import com.clientportal.api.test.utils.ClientPortal_RestAssuredRequest;
import com.clientportal.api.test.utils.ClientPortal_Utils;
import com.clientportal.api.test.utils.ClientPortal_XLUtils;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

@SuiteClasses(value = { MyDirectionPro.class })
@Listeners(value = ClientPortal_TestsListener.class)
public class MyDirectionPro {
	

	//Xlutils
	ClientPortal_XLUtils xl=new ClientPortal_XLUtils();	
	String xlfile=ClientPortal_Utils.getProperty("file.xlfile.path");
	String tcsheet = "TestData";
	
	
	ObjectMapper mapper = null;
	Map<String, String> cookies = null;
	ClientPortal_UserProfileBean results = null;
		
	@BeforeClass
	public void loadResource() {
		mapper = new ObjectMapper();
	}
	
	@Test(testName = "Post_authenticate", priority = 0)
	public void Post_authenticate() throws IOException {
		
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();
		httpRequest.basePath("/api/clientUser/authenticate");
		
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();
        
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
            
            if (keyword.equalsIgnoreCase("/api/clientUser/authenticate"))
            {	
            	String putpara1=xl.getCellData(xlfile, tcsheet, i, 4);
            	String putpara2=xl.getCellData(xlfile, tcsheet, i, 5);
            	
				// Create map of input parameters and set to request body if any!
            	Map<String, Object> params = new HashMap<String, Object>();            	
				params.put("username", putpara1);
				params.put("password", putpara2);				
				
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));

				// Send post request
				Response response = httpRequest.post();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}
	
	@Test(testName = "Get_templates", priority = 1)
	public void Get_templates() throws IOException {
		
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();
		httpRequest.basePath("/api/ditka/sponsors/templates");
		
		// Send post request
		Response response = httpRequest.get();
		ResponseBody body = response.getBody();
		System.out.println(response.getBody().asString());
				
		// Validate status code
		response.then().assertThat().statusCode(200);
			
	}

	@Test(testName = "Get_templates_components", priority = 2)
	public void Get_templates_components() throws IOException {
		
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();
			
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();
        
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
            
            if (keyword.equalsIgnoreCase("/api/ditka/sponsors/templates/{templateFileName}/components"))
            {	
            	String templateFileName=xl.getCellData(xlfile, tcsheet, i, 4);
            	           	
            	httpRequest.basePath("/api/ditka/sponsors/templates/"+templateFileName+"/components");
            	
				// Create map of input parameters and set to request body if any!
            	Map<String, Object> params = new HashMap<String, Object>();            	
				params.put("templateFileName", templateFileName);
							
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));

				// Send get request
				Response response = httpRequest.get();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}
	
	@Test(testName = "Get_configurationNames", priority = 3)
	public void Get_configurationNames() throws IOException {
		
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();
			
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();
        
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
            
            if (keyword.equalsIgnoreCase("/api/ditka/sponsors/{sponsorCode}/components/configurations/configurationNames"))
            {	
            	String sponsorCode=xl.getCellData(xlfile, tcsheet, i, 4);
            	           	
            	httpRequest.basePath("/api/ditka/sponsors/"+sponsorCode+"/components/configurations/configurationNames");
            	
				// Create map of input parameters and set to request body if any!
            	Map<String, Object> params = new HashMap<String, Object>();            	
				params.put("sponsorCode", sponsorCode);
							
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));

				// Send get request
				Response response = httpRequest.get();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}
	
	@Test(testName = "Get_configuration", priority = 4)
	public void Get_configuration() throws IOException {
		
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();
			
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();
        
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
            
            if (keyword.equalsIgnoreCase("Get_/api/ditka/sponsors/{sponsorCode}/components/configuration"))
            {	
            	String sponsorCode=xl.getCellData(xlfile, tcsheet, i, 4);
            	String Putparam2=xl.getCellData(xlfile, tcsheet, i, 5);
            	           	
            	httpRequest.basePath("/api/ditka/sponsors/"+sponsorCode+"/components/configuration");
            	
				// Create map of input parameters and set to request body if any!
            	Map<String, Object> params = new HashMap<String, Object>();            	
				params.put("sponsorCode", sponsorCode);
				params.put("configurationName", Putparam2);
							
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));

				// Send get request
				Response response = httpRequest.get();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}
	
	@Test(testName = "Post_configuration", priority = 5)
	public void Post_configuration() throws IOException {
		
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();
			
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();
        
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
            
            if (keyword.equalsIgnoreCase("Post_/api/ditka/sponsors/{sponsorCode}/components/configuration"))
            {	
            	String sponsorCode=xl.getCellData(xlfile, tcsheet, i, 4);
            	String Putparam2=xl.getCellData(xlfile, tcsheet, i, 5);
            	           	
            	httpRequest.basePath("/api/ditka/sponsors/"+sponsorCode+"/components/configuration");
            	
				// Create map of input parameters and set to request body if any!
            	Map<String, Object> params = new HashMap<String, Object>();            	
				params.put("sponsorCode", sponsorCode);
				params.put("configurationName", Putparam2);
							
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));

				// Send get request
				Response response = httpRequest.get();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}
	
	@Test(testName = "Get_coupons", priority = 6)
	public void Get_coupons() throws IOException {
		
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();
		httpRequest.basePath("/api/ditka/coupons");
		
		// Send post request
		Response response = httpRequest.get();
		ResponseBody body = response.getBody();
		System.out.println(response.getBody().asString());
				
		// Validate status code
		response.then().assertThat().statusCode(200);
			
	}
	
	@Test(testName = "Post_coupons", priority = 7,enabled=false)
	public void Post_coupons() throws IOException {
		
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();
		httpRequest.basePath("/api/ditka/coupons");
		
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();        
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
            
            if (keyword.equalsIgnoreCase("/api/ditka/coupons"))
            {	
            	String Putparam1=xl.getCellData(xlfile, tcsheet, i, 4);
            	String Putparam2=xl.getCellData(xlfile, tcsheet, i, 5);
            	String Putparam3=xl.getCellData(xlfile, tcsheet, i, 6);
            	String Putparam4=xl.getCellData(xlfile, tcsheet, i, 7);
            	String Putparam5=xl.getCellData(xlfile, tcsheet, i, 8);
            	double dPutparam5= Double.parseDouble(Putparam5);
            	String Putparam6=xl.getCellData(xlfile, tcsheet, i, 9);
            	int intPutparam6=Integer.parseInt(Putparam6);
            	
				// Create map of input parameters and set to request body if any!
            	Map<String, Object> params = new HashMap<String, Object>();            	
				params.put("issueDate", Putparam1);
				params.put("couponCode", Putparam2);
				params.put("description", Putparam3);
				params.put("adminNote", Putparam4);
				params.put("openingFeeDiscountPercentage", dPutparam5);
				params.put("dollarsOffOpeningFee", intPutparam6);
							
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));

				// Send POST request
				Response response = httpRequest.post();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}	
	
	@Test(testName = "Delete_coupons", priority = 8)
	public void Delete_coupons() throws IOException {
		
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();
				
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();        
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
            
            if (keyword.equalsIgnoreCase("/api/ditka/coupons/{id}"))
            {	
            	String id=xl.getCellData(xlfile, tcsheet, i, 4);
            	
            	httpRequest.basePath("/api/ditka/coupons/"+id+"");
				// Create map of input parameters and set to request body if any!
            	Map<String, Object> params = new HashMap<String, Object>();            	
				params.put("id", id);				
							
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));

				// Send delete request
				Response response = httpRequest.delete();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}
	
	@Test(testName = "Patch_coupons", priority = 9)
	public void Patch_coupons() throws IOException {
		
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();
				
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();        
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
            
            if (keyword.equalsIgnoreCase("Patch_/api/ditka/coupons/{id}"))
            {	
            	String id=xl.getCellData(xlfile, tcsheet, i, 4);
            	String Putparam1=xl.getCellData(xlfile, tcsheet, i, 5);
            	String Putparam2=xl.getCellData(xlfile, tcsheet, i, 6);
            	String Putparam3=xl.getCellData(xlfile, tcsheet, i, 7);
            	String Putparam4=xl.getCellData(xlfile, tcsheet, i, 8);
            	String Putparam5=xl.getCellData(xlfile, tcsheet, i, 9);
            	double dPutparam5= Double.parseDouble(Putparam5);
            	String Putparam6=xl.getCellData(xlfile, tcsheet, i, 10);
            	int intPutparam6=Integer.parseInt(Putparam6);
            	
            	httpRequest.basePath("/api/ditka/coupons/"+id+"");
				// Create map of input parameters and set to request body if any!
            	Map<String, Object> params = new HashMap<String, Object>();  
            	Map<String, Object> params1 = new HashMap<String, Object>();  
				params.put("id", id);				
				params1.put("issueDate", Putparam1);
				params1.put("couponCode", Putparam2);
				params1.put("description", Putparam3);
				params1.put("adminNote", Putparam4);
				params1.put("openingFeeDiscountPercentage", dPutparam5);
				params1.put("dollarsOffOpeningFee", intPutparam6);
				params.put("Body", mapper.writeValueAsString(params1));
				
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));

				// Send patch request
				Response response = httpRequest.patch();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}
	
	@Test(testName = "Patch_sponsors", priority = 10)
	public void Patch_sponsors() throws IOException {
		
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();
		httpRequest.basePath("/api/sponsors");
		
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();        
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
            
            if (keyword.equalsIgnoreCase("Patch_/api/sponsors"))
            {	
            	
            	String Sponsorcode=xl.getCellData(xlfile, tcsheet, i, 4);
            	String Putparam1=xl.getCellData(xlfile, tcsheet, i, 5);
            	String Putparam2=xl.getCellData(xlfile, tcsheet, i, 6);
            	String Putparam3=xl.getCellData(xlfile, tcsheet, i, 7);
            	String Putparam4=xl.getCellData(xlfile, tcsheet, i, 8);
            	String Putparam5=xl.getCellData(xlfile, tcsheet, i, 9);
            	String Putparam6=xl.getCellData(xlfile, tcsheet, i, 10);
            	String Putparam7=xl.getCellData(xlfile, tcsheet, i, 11);
            	String Putparam8=xl.getCellData(xlfile, tcsheet, i, 12);
            	         	
            	
				// Create map of input parameters and set to request body if any!
            	Map<String, Object> params = new HashMap<String, Object>();  
				params.put("sponsorCode", Sponsorcode);				
				params.put("cusip", Putparam1);
				params.put("website", Putparam2);
				params.put("description", Putparam3);
				params.put("username", Putparam4);
				params.put("password", Putparam5);
				params.put("expirationDays", Putparam6);				
				params.put("minimumAllocationAmount", Putparam7);
				params.put("maximumAllocationAmount", Putparam8);
								
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));

				// Send patch request
				Response response = httpRequest.patch();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}
	
	@Test(testName = "Post_sponsors", priority = 11)
	public void Post_sponsors() throws IOException {
		
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();
		httpRequest.basePath("/api/sponsors");
		
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();        
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
            
            if (keyword.equalsIgnoreCase("Post_/api/sponsors"))
            {	
            	
            	String Sponsorcode=xl.getCellData(xlfile, tcsheet, i, 4);
            	String Putparam1=xl.getCellData(xlfile, tcsheet, i, 5);
            	String Putparam2=xl.getCellData(xlfile, tcsheet, i, 6);
            	String Putparam3=xl.getCellData(xlfile, tcsheet, i, 7);
            	String Putparam4=xl.getCellData(xlfile, tcsheet, i, 8);
            	String Putparam5=xl.getCellData(xlfile, tcsheet, i, 9);
            	String Putparam6=xl.getCellData(xlfile, tcsheet, i, 10);
            	String Putparam7=xl.getCellData(xlfile, tcsheet, i, 11);
            	String Putparam8=xl.getCellData(xlfile, tcsheet, i, 12);
            	         	
            	
				// Create map of input parameters and set to request body if any!
            	Map<String, Object> params = new HashMap<String, Object>();  
				params.put("sponsorCode", Sponsorcode);				
				params.put("cusip", Putparam1);
				params.put("website", Putparam2);
				params.put("description", Putparam3);
				params.put("username", Putparam4);
				params.put("password", Putparam5);
				params.put("expirationDays", Putparam6);				
				params.put("minimumAllocationAmount", Putparam7);
				params.put("maximumAllocationAmount", Putparam8);
								
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));

				// Send post request
				Response response = httpRequest.post();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}
	
	@Test(testName = "Post_get_accounts", priority = 12)
	public void Post_get_accounts() throws IOException {
		
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();
		httpRequest.basePath("/api/my-direction-pro/get-accounts");
		
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();        
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
            
            if (keyword.equalsIgnoreCase("/api/my-direction-pro/get-accounts"))
            {	
            	
            	String Sponsorcode=xl.getCellData(xlfile, tcsheet, i, 4);
            	String Putparam1=xl.getCellData(xlfile, tcsheet, i, 5);
            	int intputparam1=Integer.parseInt(Putparam1);
            	String Putparam2=xl.getCellData(xlfile, tcsheet, i, 6);
            	int intputparam2=Integer.parseInt(Putparam2);
            	String Putparam3=xl.getCellData(xlfile, tcsheet, i, 7);
            	String Putparam4=xl.getCellData(xlfile, tcsheet, i, 8);
            	
            	
				// Create map of input parameters and set to request body if any!
            	Map<String, Object> params = new HashMap<String, Object>();  
				params.put("sponsorCode", Sponsorcode);				
				params.put("limit", intputparam1);
				params.put("offset", intputparam2);
				params.put("desc", true);
				params.put("sorting", Putparam3);
				params.put("sponsorRelationship", Putparam4);
				
								
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));

				// Send post request
				Response response = httpRequest.post();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}
	
	@Test(testName = "Post_completed_transactions", priority = 13)
	public void Post_completed_transactions() throws IOException {
		
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();
		httpRequest.basePath("/api/my-direction-pro/get-completed-transactions");
		
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();        
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
            
            if (keyword.equalsIgnoreCase("/api/my-direction-pro/get-completed-transactions"))
            {	
            	
            	String Sponsorcode=xl.getCellData(xlfile, tcsheet, i, 4);
            	String Putparam1=xl.getCellData(xlfile, tcsheet, i, 5);
            	int intputparam1=Integer.parseInt(Putparam1);
            	String Putparam2=xl.getCellData(xlfile, tcsheet, i, 6);
            	int intputparam2=Integer.parseInt(Putparam2);
            	String Putparam3=xl.getCellData(xlfile, tcsheet, i, 7);
            	String Putparam4=xl.getCellData(xlfile, tcsheet, i, 8);
            	            	
				// Create map of input parameters and set to request body if any!
            	Map<String, Object> params = new HashMap<String, Object>();  
				params.put("sponsorCode", Sponsorcode);				
				params.put("isAdvisor", false);
				params.put("limit", intputparam1);
				params.put("offset", intputparam2);
				params.put("desc", true);				
				params.put("sorting", Putparam3);
				params.put("sponsorRelationship", Putparam4);
								
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));

				// Send post request
				Response response = httpRequest.post();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}
	
	@Test(testName = "Post_pending_transactions", priority = 14)
	public void Post_pending_transactions() throws IOException {
		
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();
		httpRequest.basePath("/api/my-direction-pro/get-pending-transactions");
		
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();        
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
            
            if (keyword.equalsIgnoreCase("/api/my-direction-pro/get-pending-transactions"))
            {	
            	
            	String Sponsorcode=xl.getCellData(xlfile, tcsheet, i, 4);
            	String Putparam1=xl.getCellData(xlfile, tcsheet, i, 5);
            	int intputparam1=Integer.parseInt(Putparam1);
            	String Putparam2=xl.getCellData(xlfile, tcsheet, i, 6);
            	int intputparam2=Integer.parseInt(Putparam2);
            	String Putparam3=xl.getCellData(xlfile, tcsheet, i, 7);
            	String Putparam4=xl.getCellData(xlfile, tcsheet, i, 8);
            	
            	
				// Create map of input parameters and set to request body if any!
            	Map<String, Object> params = new HashMap<String, Object>();  
				params.put("sponsorCode", Sponsorcode);				
				params.put("isAdvisor", false);
				params.put("limit", intputparam1);
				params.put("offset", intputparam2);
				params.put("desc", true);				
				params.put("sorting", Putparam3);
				params.put("sponsorRelationship", Putparam4);
								
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));

				// Send post request
				Response response = httpRequest.post();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}
	
	
}
