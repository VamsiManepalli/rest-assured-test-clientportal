package com.clientportal.api.test;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.runners.Suite.SuiteClasses;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.clientportal.api.listeners.ClientPortal_TestsListener;
import com.clientportal.api.test.beans.ClientPortal_UserProfileBean;
import com.clientportal.api.test.utils.ClientPortal_RestAssuredRequest;
import com.clientportal.api.test.utils.ClientPortal_Utils;
import com.clientportal.api.test.utils.ClientPortal_XLUtils;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

@SuiteClasses(value = { SecurityQuestions.class })
@Listeners(value = ClientPortal_TestsListener.class)
public class SecurityQuestions {
	
	//Xlutils
	ClientPortal_XLUtils xl=new ClientPortal_XLUtils();	
	String xlfile=ClientPortal_Utils.getProperty("file.xlfile.path");
	String tcsheet = "TestData";
		
		
	ObjectMapper mapper = null;
	Map<String, String> cookies = null;
	ClientPortal_UserProfileBean results = null;
			
	@BeforeClass
	public void loadResource() {
	mapper = new ObjectMapper();
	}
		
	@Test(testName = "Get_security_questions", priority = 0, enabled=false)
	public void Get_security_questions() throws IOException {
			
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();
		httpRequest.basePath("/api/v1/accounts/security-questions");
		
		// Send get request
		Response response = httpRequest.get();
		ResponseBody body = response.getBody();
		System.out.println(response.getBody().asString());
					
		// Validate status code
		response.then().assertThat().statusCode(200);
		}
	
	@Test(testName = "Delete_securityquestions_answers", priority = 1,enabled=false)
	public void Delete_securityquestions_answers() throws IOException {
		
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();
		httpRequest.basePath("/api/v1/accounts/security-questions/answers");
		
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
            
            if (keyword.equalsIgnoreCase("/api/v1/accounts/security-questions/answers"))
            {	
            	String putpara1=xl.getCellData(xlfile, tcsheet, i, 4);
            	String putpara2=xl.getCellData(xlfile, tcsheet, i, 5);
            	
				// Create map of input parameters and set to request body if any!
            	Map<String, Object> params = new HashMap<String, Object>();            	
				params.put("oracleId", putpara1);
				params.put("apiKey", putpara2);				
				
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));

				// Send delete request
				Response response = httpRequest.delete();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}
	
	@Test(testName = "Get_security_questions_answers", priority = 2,enabled=false)
	public void Get_security_questions_answers() throws IOException {
		
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();
		
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
            
            if (keyword.equalsIgnoreCase("Get_/api/v1/accounts/security-questions/answers"))
            {	
            	String putpara1=xl.getCellData(xlfile, tcsheet, i, 4);
            	            	
            	httpRequest.basePath("/api/v1/accounts/security-questions/answers");
        		httpRequest.queryParam("oracleId", putpara1);
        		
				// Send get request
				Response response = httpRequest.get();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}
	
	@Test(testName = "Get_accounts_securityquestions_logs", priority = 3,enabled=false)
	public void Get_accounts_securityquestions_logs() throws IOException {
		
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();
		
		
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
            
            if (keyword.equalsIgnoreCase("Get_/api/v1/accounts/security-questions/logs"))
            {	
            	String putpara1=xl.getCellData(xlfile, tcsheet, i, 4);
            	String putpara2=xl.getCellData(xlfile, tcsheet, i, 5);
            	
            	httpRequest.basePath("/api/v1/accounts/security-questions/logs");
            	httpRequest.queryParam("oracleId", putpara1);
            	httpRequest.queryParam("apiKey", "970e66a2-76ba-11e7-b5a5-be2e44b06b34");				
        		
				// Send post request
				Response response = httpRequest.get();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}

	@Test(testName = "Post_securityquestions_logs", priority = 4)
	public void Post_securityquestions_logs() throws IOException {
		
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();
		httpRequest.basePath("/api/v1/accounts/security-questions/logs");
		
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
            
            if (keyword.equalsIgnoreCase("Post_/api/v1/accounts/security-questions/logs"))
            {	
            	String putpara1=xl.getCellData(xlfile, tcsheet, i, 4);
            	String putpara2=xl.getCellData(xlfile, tcsheet, i, 5);
            	String putpara3=xl.getCellData(xlfile, tcsheet, i, 6);
            	String putpara4=xl.getCellData(xlfile, tcsheet, i, 7);
            	
            	
            	httpRequest.basePath("/api/v1/accounts/security-questions/logs");
            	httpRequest.queryParam("oracleId", Integer.parseInt(putpara1));
            	
				// Create map of input parameters and set to request body if any!
            	Map<String, Object> params = new HashMap<String, Object>();  
            	//Map<String, Object> params1 = new HashMap<String, Object>();  
				
            	//params.put("oracleId", putpara1);
				params.put("clientSecurityQuestionId", putpara2);
				params.put("clientSecurityQuestionActionId", putpara3);
				params.put("message", putpara4);
				//params.put("log", mapper.writeValueAsString(params1));
				
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));
        		
				// Send post request
				Response response = httpRequest.post();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}
	
	@Test(testName = "Post__securityactions_logs", priority = 4)
	public void Post__securityactions_logs() throws IOException {
		
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();
		httpRequest.basePath("/api/v1/accounts/security-actions/logs");
		
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
            
            if (keyword.equalsIgnoreCase("Post_/api/v1/accounts/security-actions/logs"))
            {	
            	String putpara1=xl.getCellData(xlfile, tcsheet, i, 4);
            	String putpara2=xl.getCellData(xlfile, tcsheet, i, 5);
            	String putpara3=xl.getCellData(xlfile, tcsheet, i, 6);
            	String putpara4=xl.getCellData(xlfile, tcsheet, i, 7);
            	
            	
            	httpRequest.basePath("/api/v1/accounts/security-questions/logs");
            	httpRequest.queryParam("oracleId", Integer.parseInt(putpara1));
            	
				// Create map of input parameters and set to request body if any!
            	Map<String, Object> params = new HashMap<String, Object>();  
            	//Map<String, Object> params1 = new HashMap<String, Object>();  
				
            	//params.put("oracleId", putpara1);
				params.put("clientSecurityQuestionId", putpara2);
				params.put("clientSecurityQuestionActionId", putpara3);
				params.put("message", putpara4);
				//params.put("log", mapper.writeValueAsString(params1));
				
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));
        		
				// Send post request
				Response response = httpRequest.post();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}
	
	
}
