package com.clientportal.api.test;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.runners.Suite.SuiteClasses;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.clientportal.api.listeners.ClientPortal_TestsListener;
import com.clientportal.api.test.beans.ClientPortal_UserProfileBean;
import com.clientportal.api.test.utils.ClientPortal_RestAssuredRequest;
import com.clientportal.api.test.utils.ClientPortal_Utils;
import com.clientportal.api.test.utils.ClientPortal_XLUtils;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;
@SuiteClasses(value = { authenticate.class })
@Listeners(value = ClientPortal_TestsListener.class)


public class ClientUser
{

	//Xlutils
	ClientPortal_XLUtils xl=new ClientPortal_XLUtils();	
	String xlfile=ClientPortal_Utils.getProperty("file.xlfile.path");
	String tcsheet = "TestData";
	ObjectMapper mapper = null;
	Map<String, String> cookies = null;
	ClientPortal_UserProfileBean results = null;
		
	@BeforeClass
	public void loadResource() {
		mapper = new ObjectMapper();
	}
	@Test(testName = "ClientUsersAllocationTest", priority = 0)
	public void clientUsersAllocation() throws IOException 
	{
		RequestSpecification httpRequest = ClientPortal_RestAssuredRequest.getInstance();
		FileInputStream fi=new FileInputStream(xlfile);
		Workbook wb=new XSSFWorkbook(fi);
		org.apache.poi.ss.usermodel.Sheet ws=wb.getSheet(tcsheet);
        int rc=ws.getLastRowNum();
        
        for (int i = 0; i <=rc; i++) 
        {
        	String keyword = xl.getCellData(xlfile,tcsheet , i, 2);
        	
            if (keyword.equalsIgnoreCase("/api/v2/client-users/{accountNumber}/allocations"))
            {	
            	String putpara1=xl.getCellData(xlfile, tcsheet, i, 4);
            	String putpara2=xl.getCellData(xlfile, tcsheet, i, 5);
            	String putpara3=xl.getCellData(xlfile, tcsheet, i, 6);
            	String putpara4=xl.getCellData(xlfile, tcsheet, i, 7);
            	String putpara5=xl.getCellData(xlfile, tcsheet, i, 8);
            	String putpara6=xl.getCellData(xlfile, tcsheet, i, 9);
            	String putpara7=xl.getCellData(xlfile, tcsheet, i, 10);
            	String putpara8=xl.getCellData(xlfile, tcsheet, i, 11);
            	String putpara9=xl.getCellData(xlfile, tcsheet, i, 12);
            	String putpara10=xl.getCellData(xlfile, tcsheet, i, 13);
            	String putpara11=xl.getCellData(xlfile, tcsheet, i, 14);
            	String putpara12=xl.getCellData(xlfile, tcsheet, i, 15);
            	
            	httpRequest.basePath("/api/v2/client-users/"+putpara1+"/allocations");

				// Create map of input parameters and set to request body if any!
            	Map<String, Object> params = new HashMap<String, Object>();
            	Map<String, Object> params1 = new HashMap<String, Object>();
            	params.put("accountNumber", putpara1);
            	params1.put("sponsorName",putpara2);
            	params1.put("sponsorContact",putpara3);
            	params1.put("email",putpara4);
            	params1.put("phone",putpara5);
            	params1.put("allocationAmount",Integer.parseInt(putpara6));
            	params1.put("investmentType",Integer.parseInt(putpara7));
            	params1.put("notes",putpara8);
            	params1.put("allAvailableFunds",putpara9);
            	params1.put("depositoryInfo",putpara10);
            	params1.put("storageInfo",putpara11);
            	params1.put("transactionFees",putpara12);
            	params.put("Allocation Request", params1);
            	System.out.println(params);
            	
				// Convert Java Object to JSON string
				httpRequest.body(mapper.writeValueAsString(params));

				// Send post request
				Response response = httpRequest.post();
				ResponseBody body = response.getBody();
				System.out.println(response.getBody().asString());
				// Validate status code
				response.then().assertThat().statusCode(200);
			}
        }
	}
}