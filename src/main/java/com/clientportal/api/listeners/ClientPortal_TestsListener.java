package com.clientportal.api.listeners;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.clientportal.api.test.beans.ClientPortal_XLReportBean;
import com.clientportal.api.test.utils.ClientPortal_TestStatus;
import com.clientportal.api.test.utils.ClientPortal_Utils;
import com.clientportal.api.test.utils.ClientPortal_XLRepostGenerator;

public class ClientPortal_TestsListener implements ITestListener {

	private static final String FILE_NAME = ClientPortal_Utils.getProperty("file.xlfile.path");
	private ClientPortal_XLRepostGenerator xlReport = null;
	private List<ClientPortal_XLReportBean> testResults = null;
	private List<ClientPortal_XLReportBean> Result = null;
	public void onFinish(ITestContext arg0) {
		// TODO Auto-generated method stub
		List<ITestResult> allResults = new ArrayList<ITestResult>(arg0.getPassedTests().getAllResults());
		allResults.addAll(arg0.getFailedTests().getAllResults());
		allResults.addAll(arg0.getSkippedTests().getAllResults());
		System.out.println(arg0.getSuite().getName());
		testResults = new ArrayList<ClientPortal_XLReportBean>();
		for (ITestResult result : allResults) {
			ClientPortal_XLReportBean reportBean = new ClientPortal_XLReportBean();
			reportBean.setTestName(result.getName());
			reportBean.setStatus(ClientPortal_TestStatus.getTestStatusName(result.getStatus()));
			if(result.getThrowable() != null) {
				reportBean.setErrorMessage(result.getThrowable().getMessage());
			}
			reportBean.setStartDate(new Date(result.getStartMillis()));
			reportBean.setEndDate(new Date(result.getEndMillis()));
			testResults.add(reportBean);
		}
		
		xlReport.createXLFile(FILE_NAME, testResults);
		//xlReport.closeWorkBook();
	}

	public void onStart(ITestContext arg0) {
		// TODO Auto-generated method stub
		xlReport = new ClientPortal_XLRepostGenerator();
		try {
			xlReport.createWorkBook();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public void onTestFailedButWithinSuccessPercentage(ITestResult arg0) {
		// TODO Auto-generated method stub
	}

	public void onTestFailure(ITestResult arg0) {
		// TODO Auto-generated method stub
	}

	public void onTestSkipped(ITestResult arg0) {
		// TODO Auto-generated method stub
	}

	public void onTestStart(ITestResult arg0) {
		// TODO Auto-generated method stub
	}

	public void onTestSuccess(ITestResult arg0) {
		// TODO Auto-generated method stub
	}

}
